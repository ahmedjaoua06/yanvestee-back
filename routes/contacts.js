var express = require('express');
var router = express.Router();
var contacts = require('../controllers/contactsController');
router.post('/add',contacts.addContact);
router.post('/update',contacts.updateContact);
router.post('/delete',contacts.deleteContact);
router.post('/',contacts.mesContacts);
router.post('/invite',contacts.inviteContacts);
module.exports = router;