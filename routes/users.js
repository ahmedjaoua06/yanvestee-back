var multer = require("multer");
const mongoose = require("mongoose");
const passport = require("passport");
const auth = require("./auth");
var Users = require("../models/user");
var nodeMailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var jade = require("jade");
var fs = require("fs");

var storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, __dirname + "./../public/uploads");
  },
  filename: function(req, file, callback) {
    console.log(file);
    let t = new Date().getTime();
    console.log(t);
    var str = file.originalname;
    var n = str.lastIndexOf(".");
    var result = str.substring(n + 1);
    callback(null, t + "." + result);
  }
});
var upload = multer({ storage: storage });
const router = require("express").Router();
router.post("/upload", upload.any(), (req, res) => {
  var files = req.files;
  console.log(req.files);
  var docs = [];
  console.log("======");
  files.forEach(file => {
    // console.log(file.filename);
    // console.log(file.originalname);
    docs.push({
      original_name: file.originalname,
      new_name: file.filename
    });
  });
  console.log(docs[0]);
  res.send(docs[0]);
});

//POST new user route (optional, everyone has access)
router.post("/", auth.optional, (req, res, next) => {
  /* const {
    body: { user }
  } = req;*/
  var user = req.body.user;
  var domaine = req.body.domaine;
  console.log(user);
  Users.findOne({ email: user.email }, function(err, data) {
    if (data) {
      return res.sendStatus(400);
    }
  });
  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required"
      }
    });
  }

  const finalUser = new Users(user);

  finalUser.setPassword(user.password);

  var template = process.cwd() + "/views/register.jade";
  fs.readFile(template, "utf8", function(err, file) {
    if (err) {
      console.log(err);
      return res.json({ result: "error" + err });
    } else {
      var compiledTmpl = jade.compile(file, { filename: template });
      var context = {
        link: domaine + "/users?verif=" + finalUser._id,
        user: finalUser
      };

      // get html back as a string with the context applied;
      var html = compiledTmpl(context);
      let transporter = nodeMailer.createTransport({
        service: "gmail",
        auth: {
          user: "inspinia.am@gmail.com",
          pass: "inspinia2019"
        }
      });

      let mailOptions = {
        from: "inspinia.am@gmail.com", // sender address
        to: "mahmoud.ellouze911@gmail.com", // list of receivers
        subject: "Vérification d'inscription", // Subject line
        text: req.body.body, // plain text body
        html: html // html body
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
        } else {
          console.log("Message Sent!");
        }
      });
    }
  });

  return finalUser
    .save()
    .then(() => res.json({ user: finalUser.toAuthJSON() }));
});

//POST login route (optional, everyone has access)
router.post("/login", auth.optional, (req, res, next) => {
  const {
    body: { user }
  } = req;
  //user = req.body
  if (!user.email) {
    return res.status(422).json({
      errors: {
        email: "is required"
      }
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: "is required"
      }
    });
  }

  return passport.authenticate(
    "local",
    { session: false },
    (err, passportUser, info) => {
      if (err) {
        return next(err);
      }

      if (passportUser) {
        const user = passportUser;
        user.token = passportUser.generateJWT();
        if (user.verif == false) {
          return res.json({ verifError: "error logging in" });
        }
        return res.json({ user: user.toAuthJSON() });
      }

      //return status(400).info;
      return res.json({ error: "error logging in" });
    }
  )(req, res, next);
});

//GET current route (required, only authenticated users have access)
router.get("/current", auth.required, (req, res, next) => {
  const {
    payload: { id }
  } = req;

  return Users.findById(id).then(user => {
    if (!user) {
      return res.sendStatus(400);
    }
    return res.json({ user: user.toAuthJSON() });
  });
});

router.post("/verification", function(req, res) {
  Users.findOneAndUpdate({ _id: req.body.id }, { verif: true }, function(
    error,
    user
  ) {
   
    var template = process.cwd() + "/views/confirmation.jade";
    fs.readFile(template, "utf8", function(err, file) {
      if (err) {
        console.log(err);
        return res.json({ result: "error" + err });
      } else {
        var compiledTmpl = jade.compile(file, { filename: template });
        var context = {
          link: req.body.domaine + "/users",
        };
  
        // get html back as a string with the context applied;
        var html = compiledTmpl(context);
        let transporter = nodeMailer.createTransport({
          service: "gmail",
          auth: {
            user: "inspinia.am@gmail.com",
            pass: "inspinia2019"
          }
        });
  
        let mailOptions = {
          from: "inspinia.am@gmail.com", // sender address
          to: user.email, // list of receivers
          subject: "Vérification d'inscription", // Subject line
          text: req.body.body, // plain text body
          html: html // html body
        };
  
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
          } else {
            console.log("Message Sent!");
          }
        });
      }
    });
    if (!error) return res.send({ msg: "compte est verifier" });
    // return res.sendStatus(400).json({msg:"compte n est pas verifier"});
  });
});

module.exports = router;
