var multer = require('multer')
const auth = require('./auth')
var nodeMailer = require('nodemailer')
var smtpTransport = require('nodemailer-smtp-transport')
var jade = require('jade');
var fs = require('fs');

var storage = multer.diskStorage({
	destination: function(req, file, callback) {
		callback(null, __dirname+'./../public/uploads')
    },
	filename: function(req, file, callback) {
		console.log(file)
		let t = (new Date()).getTime()
		console.log(t)
		var str = file.originalname;
		var n = str.lastIndexOf('.');
		var result = str.substring(n + 1);
		callback(null, t+'.'+result)
	}
})
var upload = multer(({ storage: storage }));

var express = require('express');
var router = express.Router();
var emissions = require('../controllers/emissionsController');
router.post('/upload/:doc_type/:id', upload.any(),emissions.uploadDocs);
router.post('/',emissions.addEmission);
router.get('/byemtteur/:emetteur_id',emissions.getEmissionsByUser)
router.post('/updateEmissionEmprunt/:id',emissions.updateEmissionEmprunt);
router.post('/updateEmissionSociete/:id',emissions.updateEmissionSociete);
router.post('/updateEmissionFinal/:id',emissions.updateEmissionFinal);

router.get('/public',emissions.getPublicEmissions)
router.post('/private',emissions.getPrivateEmissions)

router.get('/sort/:status', emissions.getEmissionEnCours)
router.get('/filterbydate/:order', emissions.sortByDateEcheance)
router.get('/:id',emissions.getEmissionById)
router.post('/investir',emissions.invest)


router.post('/filerbysociete',emissions.filterBySociete);

router.post('/filterByMontantEmis',emissions.filterByMontantEmis);
router.post('/filterBySouscrit',emissions.filterBySouscrit);

// mes emissions
router.get('/me/sort/:status/:id', emissions.getMesEmissionEnCours)
router.get('/me/filterbydate/:order/:id', emissions.sortByDateEcheanceMes)
router.post('/me/filerbysociete',emissions.filterBySocieteMes);
router.post('/me/filterByMontantEmis',emissions.filterByMontantEmisMes);
router.post('/me/filterBySouscrit',emissions.filterBySouscritMes);

router.post('/certifDepot',emissions.addCertif);
router.post('/certifDepot/all',emissions.getCertif);
router.post('/billetTresor',emissions.addBillet);
router.post('/billetTresor/all',emissions.getBillet);
router.post('/compte',emissions.addCompte);
router.post('/compte/all',emissions.getCompte);

router.post('/sendEmail', function (req, res) {
	var template = process.cwd() + '/views/mail.jade';
	  // get template from file system
	  fs.readFile(template, 'utf8', function (err, file) {
		if (err) {
		  //handle errors
		  console.log( err);
		  return res.json({ result: "error" + err });
		}
		else {
		  //compile jade template into function
		  var compiledTmpl = jade.compile(file, { filename: template });
		  // set context to be used in template
	
		  //var codeint = randomInt(1000, 9999);
	
		  var context = { 
			  link: 'http://localhost:4200/app/emissions/detail/'+req.body.id , 
			  name : req.body.emetteur};
	
		  // get html back as a string with the context applied;
		  var html = compiledTmpl(context);
		  let transporter = nodeMailer.createTransport({
			service: 'gmail',
			auth: {
				user: 'inspinia.am@gmail.com',
				pass: 'inspinia2019'
			}
		})
	
		let mailOptions = {
			from: 'inspinia.am@gmail.com', // sender address
			to: req.body.to, // list of receivers
			subject: 'Invitation Investissement - INSPINIA', // Subject line
			text: req.body.body, // plain text body
			html: html // html body
		};
	
	
	
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				res.send(error)
			}else{
				console.log('Message Sent!');
				res.send({msg:'email sent!'})
			}
			});
		 
		}
	  });
	
	});

	router.post('/notifyEmetteur', function (req, res) {
		var template = process.cwd() + '/views/investmail.jade';
		  // get template from file system
		  fs.readFile(template, 'utf8', function (err, file) {
			if (err) {
			  //handle errors
			  console.log( err);
			  return res.json({ result: "error" + err });
			}
			else {
			  //compile jade template into function
			  var compiledTmpl = jade.compile(file, { filename: template });
			  // set context to be used in template
		
			  //var codeint = randomInt(1000, 9999);
		
			  var context = { 
				  link: 'http://localhost:4200/app/emissions/detail/'+req.body.id , 
				  name : req.body.investisseur,
				  montant: req.body.montant};
		
			  // get html back as a string with the context applied;
			  var html = compiledTmpl(context);
			  let transporter = nodeMailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'inspinia.am@gmail.com',
					pass: 'inspinia2019'
				}
			})
		
			let mailOptions = {
				from: 'inspinia.am@gmail.com', // sender address
				to: req.body.to, // list of receivers
				subject: 'Demande pour invest', // Subject line
				text: req.body.body, // plain text body
				html: html // html body
			};
		
		
		
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					res.send(error)
				}else{
					console.log('Message Sent!');
					res.send({msg:'email sent!'})
				}
				});
			 
			}
		  });
		
		});
	
module.exports = router;