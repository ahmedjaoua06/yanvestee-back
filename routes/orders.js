var express = require('express');
var router = express.Router();
var orders = require('../controllers/ordersController');
router.post('/', orders.addOrder)
router.get('/', orders.getOrders)
router.get('/:id', orders.getOrdersById)


router.post('/offer/add',orders.addOffer)
router.post('/offer/decline',orders.decline)
router.post('/offer/accept',orders.acceptt)
router.post('/offer/acceptp',orders.accepttP)
router.get('/mesoffres/:id',orders.mesOffres)
router.post('/banks',orders.banks)
module.exports = router;