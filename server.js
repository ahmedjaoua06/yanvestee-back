var createError = require('http-errors');
var express = require('express');
var path = require('path');
require('dotenv').config();
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var mongoose = require('mongoose');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var bodyParser = require('body-parser');

var routes = require('./app/routers');

var mongoAdress = 'mongodb://' + process.env.DB_HOST + ':' + process.env.DB_PORT + "/" + process.env.DB_NAME
mongoose.connect(mongoAdress, function (err, result) {
  if (err)
    return console.log("Could not connect", err);
  else
    return console.log("Connected to database");
});
require('./config/passport');

var app = express();
app.use(cors());


app.use(bodyParser.urlencoded({
  limit: '5mb',
  parameterLimit: 100000,
  extended: false
}));

app.use(bodyParser.json({
  limit: '5mb'
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.use('/static', express.static('public'))

app.use('/api', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
var http = require('http');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.SERVER_PORT || 3000);
app.set('port', port);

/**
 * Create HTTP server.
 */
console.log('The magic happens on port ' + process.env.SERVER_PORT);
var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}


module.exports = app;
