var mongoose = require("mongoose");

var ContactSchema = new mongoose.Schema({
  firstname: { type: String },
  lastname: { type: String },
  email: { type: String },
  tel: { type: String },
  address: { type: String },
  fonction: { type: String },
  societe: { type: String },
  categorie: { type: String },
  note: { type: String },
  user_id :  { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
});

module.exports = mongoose.model("Contacts", ContactSchema);
