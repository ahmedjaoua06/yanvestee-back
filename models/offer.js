var mongoose = require("mongoose");

var OfferSchema = new mongoose.Schema({
  banque_id: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
  order_id: { type: mongoose.Schema.Types.ObjectId, ref: "Orders" },
  taux: { type: Number },
  montant: { type: Number },
  status: { type: String, default: 'encours' },
  create_date: { type: Date, default: new Date()},
  compteAterme: { type: Boolean, default: false },
  pensionLivre: { type: Boolean, default: false },
  certificatDeDepot: { type: Boolean, default: false },
  billetTresorerie: { type: Boolean, default: false },
});

module.exports = mongoose.model("Offers", OfferSchema);