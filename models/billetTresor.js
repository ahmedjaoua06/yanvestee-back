var mongoose = require("mongoose");

var BillettrisorSchema = new mongoose.Schema({
  
  montant: { type: Number },
  duree: { type: Number },
  taux: { type: Number },
  isfinished: { type: Boolean, default: false },
  emetteur_id: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
  create_date: { type: Date, default: new Date()},
  public: { type: Boolean, default: true },
  
});

module.exports = mongoose.model("BilletTresors", BillettrisorSchema);
