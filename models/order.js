var mongoose = require("mongoose");

var OrderSchema = new mongoose.Schema({
  
  montant: { type: Number },
  duree: { type: Number },
  date_dispo: { type: String },
  isfinished: { type: Boolean, default: false },
  offres: [
    { type: mongoose.Schema.Types.ObjectId, ref: "Offers" }
  ],
  invisible: [
    { type: mongoose.Schema.Types.ObjectId, ref: "Users" }
  ],
  investisseur_id: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
  create_date: { type: Date, default: new Date()},
  compteAterme: { type: Boolean, default: false },
  pensionLivre: { type: Boolean, default: false },
  certificatDeDepot: { type: Boolean, default: false },
  billetTresorerie: { type: Boolean, default: false },
});

module.exports = mongoose.model("Orders", OrderSchema);
