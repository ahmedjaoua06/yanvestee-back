var mongoose = require('mongoose');

var mongoHost = process.env.MONGO_HOST || "localhost";
var mongoPort = process.env.MONGO_PORT || "27017";

var url = "mongodb://"+mongoHost+":"+mongoPort+"/inspiniadb1";

console.log("Trying to conntec mongodb:", url);
mongoose.connect(url, function(err, result){
    if(err)
        return console.log("Could not connect", err);
    else
    return console.log("connected to database");
});

module.exports = mongoose;