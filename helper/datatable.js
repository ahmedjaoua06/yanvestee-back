var _ = require('lodash');
var moment = require('moment');
module.exports = function (req, data) {
    //if request.body is undefined we do not apply the script
    if (!req || !req.body.meta) return data;
    var table = {
        size: req.body.meta.size || 0,
        totalElements: req.body.meta.totalElements || 0,
        totalPages: req.body.meta.totalPages || 0,
        pageNumber: req.body.meta.pageNumber || 0,
        sortDir: req.body.meta.sortDir || 'asc',
        sortProperty: req.body.meta.sortProperty || '',
        searches: req.body.meta.searches || [],
        searchesIn: req.body.meta.searchesIn || [],
        search: req.body.meta.search || ''
    };
    //global search
    if (!_.isEmpty(table.search) && _.isString(table.search)) {
        data = data.filter(function (object) {
            for (var field in object) {
                if (field == '_id') continue;
                var exist = false;
                if (_.isString(object[field])) {
                    exist = object[field].toLowerCase().indexOf(table.search) > -1;
                }
                else if (_.isArray(object[field]) && !_.isEmpty(object[field])) {
                    var contains = false;
                    var array = object[field];
                    for (var i = 0; i < array.length; i++) {
                        var element = array[i];
                        if (_.isString(element)) {
                            contains = element.toLowerCase().indexOf(table.search) > -1;
                        }
                        if (contains) break;
                    }
                    exist = contains;
                }
                else if (_.isObject(object[field]) && !_.isEmpty(object[field])) {
                    var obj = object[field];
                    var found = false;
                    for (var key in obj) {
                        if (key == '_id') continue;
                        if (obj.hasOwnProperty(key)) {
                            if (_.isString(obj[key]) && !_.isEmpty(obj[key])) {
                                found = obj[key].toLowerCase().indexOf(table.search.toLowerCase()) > -1;
                            }
                            if (_.isObject(obj[key]) && !_.isEmpty(obj[key])) {
                                var subObj = obj[key];
                                for (var koo in subObj) {
                                    if (koo == '_id') continue;
                                    if (subObj.hasOwnProperty(koo)) {
                                        if (_.isString(subObj[koo]) && !_.isEmpty(subObj[koo])) {
                                            found = subObj[koo].toLowerCase().indexOf(table.search.toLowerCase()) > -1;
                                        }
                                        if (found) break;
                                    }
                                }
                            }
                            if (found) break;
                        }
                    }
                    exist = found;
                }
                if (exist) break;
            }
            return exist;
        });
    }
    //searching in column
    if (_.isArray(table.searches) && !_.isEmpty(table.searches)) {
        table.searches.forEach(function (object) {
            if (object.type == 'string' && _.isString(object.value) && !_.isEmpty(object.value)) {
                var allValues = [];
                if (object.value.includes(',')) {
                    allValues = object.value.split(",");
                }
                else {
                    allValues = [object.value]
                }
                data = _.filter(data, function (o) {
                    if (_.isString(o[object.property]) && !_.isEmpty(o[object.property])) {
                        var found = false;
                        allValues.forEach(function(v) {
                            if(v && o[object.property].toLowerCase().indexOf(v) > -1) {
                                found = true;
                            }
                        })
                        return found;
                    } else {
                        return false;
                    }
                })
            }
            else if(object.type == 'number' && _.isNumber(parseInt(object.value)) && !_.isEmpty(object.value)) {
                data = _.filter(data, function (o) {
                    if (_.isNumber(o[object.property])) {
                        return o[object.property].toString().indexOf(object.value) > -1
                    } else {
                        return false;
                    }
                })
            }
            else if (object.type == 'date' && moment(object.value).isValid() && !_.isEmpty(object.value)) {
                data = _.filter(data, function (o) {
                    if (moment(o[object.property]).isValid()) {
                        return moment(o[object.property]).isSame(object.value, 'day');
                    } else {
                        return false;
                    }
                })
            }
            else if (object.type == 'object' && _.isString(object.value) && !_.isEmpty(object.value)) {
                data = _.filter(data, function (o) {
                    if (_.isObject(o[object.property]) && !_.isEmpty(o[object.property])) {
                        var found = false;
                        var obj = o[object.property];
                        for (var key in obj) {
                            if (key == '_id') continue;
                            if (obj.hasOwnProperty(key)) {
                                if (_.isString(obj[key]) && !_.isEmpty(obj[key])) {
                                    found = obj[key].toLowerCase().indexOf(object.value.toLowerCase()) > -1;
                                }
                                if (_.isObject(obj[key]) && !_.isEmpty(obj[key])) {
                                    var subObj = obj[key];
                                    for (var koo in subObj) {
                                        if (koo == '_id') continue;
                                        if (subObj.hasOwnProperty(koo)) {
                                            if (_.isString(subObj[koo]) && !_.isEmpty(subObj[koo])) {
                                                found = subObj[koo].toLowerCase().indexOf(object.value.toLowerCase()) > -1;
                                            }
                                            if (found) break;
                                        }
                                    }

                                }
                                if (found) break;
                            }
                        }
                        return found;
                    } else {
                        return false;
                    }
                })
            }
        })
    }
    ;
    //searching in column ( when column value exist in array)
    if (_.isArray(table.searchesIn) && !_.isEmpty(table.searchesIn)) {
        table.searchesIn.forEach(function (object) {
            if (_.isArray(object.value) && !_.isEmpty(object.value)) {
                //getting the property name;
                if (_.isString(object.property) && !_.isEmpty(object.property)) {
                    var prop = '';
                    var subProp = '';
                    if (object.property.indexOf('.') > -1) {
                        var array = object.property.split('.');
                        prop = array[0];
                        subProp = array[1];
                    } else {
                        prop = object.property;
                    }
                    data = _.filter(data, function (o) {
                        if (_.isEmpty(o[prop])) {
                            return false;
                        } else if (_.isString(o[prop])) {
                            return object.value.indexOf(o[prop]) > -1;
                        } else if (_.isObject(o[prop]) && subProp && !_.isEmpty(o[prop][subProp])) {
                            return object.value.indexOf(o[prop][subProp]) > -1;
                        }
                    })
                }
            }
        })
    }
    ;
    //sorting the result
    if (table.sortProperty) {
        data = _.orderBy(data, table.sortProperty, table.sortDir);
    }
    //paginating the result
    table.totalElements = data.length;
    if (table.size > 0) {
        table.totalPages = Math.ceil(table.totalElements / table.size); // calculate total pages
        table.pageNumber = Math.max(table.pageNumber, 0); // get 1 page when pageNumber <= 0
        table.pageNumber = Math.min(table.pageNumber, table.totalPages); // get last page when pageNumber > totalPages
        var offset = table.pageNumber * table.size;
        if (offset < 0) {
            offset = 0;
        }
        data = data.slice(offset, offset + table.size);
    }
    //filling meta object for datatable
    var meta = {
        size: table.size,
        totalElements: table.totalElements,
        totalPages: table.totalPages,
        pageNumber: table.pageNumber,
        sortDir: table.sortDir,
        sortProperty: table.sortProperty,
        searches: table.searches,
        search: table.search
    };
    return {
        meta: meta,
        data: data
    }
}