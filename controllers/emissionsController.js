var mongoose = require("mongoose");
var User = require("../models/user");
var Emission = require("../models/emission");
var certifDepot = require ("../models/certifdepot");
var CompteTerme = require("../models/compteTerme");
var BilletTresor = require("../models/billetTresor");
var jade = require('jade');
var fs = require('fs');
var nodeMailer = require('nodemailer')
//update Emission with an uploaded document along with its type
module.exports.uploadDocs = function(req, res) {
  var files = req.files;
  var docs = [];
  console.log("======");
  files.forEach(file => {
    console.log(file.filename);
    console.log(file.originalname);
    docs.push({
      doc_type: req.params.doc_type,
      original_name: file.originalname,
      new_name: file.filename
    });
  });

  Emission.findById(req.params.id, function(err, emission) {
    if (!emission) return next(new Error("Could not load Document Emission"));
    else {
      emission.docs.push(docs[0]);
      emission.save(function(err) {
        if (err) res.json(err);
        else res.json(emission);
      });
    }
  });
};

module.exports.addEmission = function(req, res) {
  var new_emission = new Emission(req.body);
  new_emission.save(function(err, emission) {
    if (err) res.send(err);
    else {
      res.json(emission);
    }
  });
};

module.exports.getEmissionsByUser = function(req, res) {
  var user_id = req.params.emetteur_id;
  Emission.find({ emetteur_id: user_id }).then(emission => {
    if (emission != null) res.json(emission);
    else res.json({ msg: "notfound" });
  });
};

//update emission with emprunt object
module.exports.updateEmissionEmprunt = function(req, res) {
  var emprunt = req.body;
  Emission.findById(req.params.id, function(err, emission) {
    if (!emission) return next(new Error("Could not load Document Emission"));
    else {
      emission.emprunt = emprunt;
      emission.save(function(err) {
        if (err) res.send({ msg: "error" });
        else res.send(emission);
      });
    }
  });
};

//update emission with societe_id
module.exports.updateEmissionSociete = function(req, res) {
  var societe = req.body.societe_id;
  Emission.findById(req.params.id, function(err, emission) {
    if (!emission) return next(new Error("Could not load Document Emission"));
    else {
      emission.societe_id = societe;
      emission.save(function(err) {
        if (err) res.send({ msg: "error" });
        else res.send(emission);
      });
    }
  });
};

//update Emission final step with all props
module.exports.updateEmissionFinal = function(req, res) {
  var newEmission = req.body;
  //newEmission._id = req.params.id
  Emission.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true },
    function(err, emission) {
      if (!emission) res.send("Could not load Document Emission");
      else {
        res.send(emission);
      }
    }
  );
};

module.exports.getPublicEmissions = function(req, res) {
  Emission.find({ ispublic: true, status_cloture: false }) .exec(function(
    err,
    data
  ) {
    
    data.forEach(element => {
      if(!element.emprunt.dateCloture){
    //    delete element ;
      //  console.log("dellaa is here")
         Emission.deleteOne({_id : element._id},function(err,del){
          if (err) res.json("error on delete");
        }) 
      }
      if (Date.parse(element.emprunt.dateCloture) < Date.now()) {
        Emission.findOneAndUpdate(
          { _id: element._id },
          { status_cloture: true },
          function(err, emission) {}
        );
      }
    });
    Emission.find({ ispublic: true })
    .populate("societe_id")
    .sort({ _id: "desc" })
    .exec(function(err, data) {
       
      if (!data) res.json("no public emissions");
      else 
      var newData  = data.filter(el => el.emprunt.dateCloture)
    /*  data.forEach(element => {
        if(!element.emprunt.dateCloture){
         delete element ;
         console.log("dellaa is here")
           Emission.deleteOne({_id : element._id},function(err,del){
            if (err) res.json("error on delete");
          }) 
        }
    
      });*/
      res.json(newData);
    });
  });


};

// Deal club
module.exports.getPrivateEmissions = function(req, res) {
  Emission.find({ ispublic: false}) .exec(function(
    err,
    data
  ) {
    
    data.forEach(element => {
     /* if(!element.emprunt.dateCloture){
    //    delete element ;
      //  console.log("dellaa is here")
         Emission.deleteOne({_id : element._id},function(err,del){
          if (err) res.json("error on delete");
        }) 
      }*/
      if (Date.parse(element.emprunt.dateCloture) < Date.now()) {
        Emission.findOneAndUpdate(
          { _id: element._id },
          { status_cloture: true },
          function(err, emission) {}
        );
      }
    });
    Emission.find({ ispublic: false })
    .populate("societe_id")
    .sort({ _id: "desc" })
    .exec(function(err, data) {
       
      if (!data) res.json("no public emissions");
      else 
      var newData  = data.filter(el => el.emprunt.dateCloture)

      res.json(newData);
    });
  });


};

module.exports.getEmissionById = function(req, res) {
  Emission.findById(req.params.id)
    .populate("emetteur_id")
    .populate("societe_id")
    .exec(function(err, data) {
      if (!data) res.json("no emission with the given ID");
      else {
        User.populate(data.souscriptions, { path: "investisseur_id" }, function(
          err,
          user
        ) {
          //console.log(user)
         // var newData  = data.filter(el => el.emprunt.dateCloture)
          res.json(data);
        });
      }
    });
};

//update emission with invest infos
module.exports.invest = function(req, res) {
  //var emprunt = req.body;
  console.log(req.body);
  if(req.body.montant <=0){
    res.status(405);
  }
  Emission.findById(req.body.id, function(err, emission) {
    if (!emission) return next(new Error("Could not load Document Emission"));
    else {
      emission.souscriptions.push({
        investisseur_id: req.body.investisseur_id,
        montant: req.body.montant,
        taux: req.body.taux,
        categorie : req.body.categorie
      });
      emission.montant_souscrit = emission.montant_souscrit + req.body.montant;

      //Send E-mail starts here
      var template = process.cwd() + '/views/investmail.jade';
		  // get template from file system
		  fs.readFile(template, 'utf8', function (err, file) {
			if (err) {
			  //handle errors
			  console.log( err);
			  return res.json({ result: "error" + err });
			}
			else {
			  //compile jade template into function
			  var compiledTmpl = jade.compile(file, { filename: template });
			  // set context to be used in template
		
			  //var codeint = randomInt(1000, 9999);
		
			  var context = { 
				  link: 'http://localhost:4200/app/emissions/detail/'+req.body.id , 
				  name : req.body.investisseur,
				  montant: req.body.montant};
		
			  // get html back as a string with the context applied;
			  var html = compiledTmpl(context);
			  let transporter = nodeMailer.createTransport({
				service: 'gmail',
				auth: {
					user: 'inspinia.am@gmail.com',
					pass: 'inspinia2019'
				}
			})
		
			let mailOptions = {
				from: 'inspinia.am@gmail.com', // sender address
				to: req.body.to, // list of receivers
				subject: 'Demande Investissement - INSPINIA', // Subject line
				text: req.body.body, // plain text body
				html: html // html body
			};
		
		
		
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					return error;
				}else{
          console.log('Message Sent!');
          return {msg:'email sent!'}
				
				}
				});
			 
			}
		  });
	

      //Send E-mail ends here

      emission.save(function(err) {
        if (err) res.send({ msg: "error" });
        else res.send(emission);
      });
    }
  });
};

// filter by societe

module.exports.filterBySociete = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ ispublic: true })
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.societe_id.raison_sociale.toLowerCase(),
          nameB = b.societe_id.raison_sociale.toLowerCase();
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");

      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

// filter by montantEmis

module.exports.filterByMontantEmis = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ ispublic: true })
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.emprunt.montant,
          nameB = b.emprunt.montant;
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

// filter by souscrit

module.exports.filterBySouscrit = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ ispublic: true })
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.montant_souscrit / a.emprunt.montant,
          nameB = b.montant_souscrit / b.emprunt.montant;
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

module.exports.getEmissionEnCours = function(req, res) {
  Emission.find({ status_cloture: req.params.status, ispublic: true })
    .populate("societe_id")
    .exec(function(err, data) {
      if (!data) res.json("no emissions en cours");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

module.exports.sortByDateEcheance = function(req, res) {
  let ord = "desc";
  if (req.params.order == "true") ord = "desc";
  else ord = "asc";
  Emission.find({ ispublic: true })
    .populate("societe_id")
    .sort({ "emprunt.dateEcheance": ord })
    .exec(function(err, data) {
      if (!data) res.json("no emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};




// filter by societe   mes Emissions 

module.exports.filterBySocieteMes = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ emetteur_id : req.body.id_user })
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.societe_id.raison_sociale.toLowerCase(),
          nameB = b.societe_id.raison_sociale.toLowerCase();
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

// filter by montantEmis

module.exports.filterByMontantEmisMes = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ emetteur_id : req.body.id_user})
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.emprunt.montant,
          nameB = b.emprunt.montant;
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

// filter by souscrit

module.exports.filterBySouscritMes = function(req, res) {
  var sort = req.body.sort;
  Emission.find({ emetteur_id : req.body.id_user })
    .populate("societe_id")
    .exec(function(err, data) {
      data.sort(function(a, b) {
        var nameA = a.montant_souscrit / a.emprunt.montant,
          nameB = b.montant_souscrit / b.emprunt.montant;
        if (sort) {
          if (nameA < nameB) return 1;
          if (nameA > nameB) return -1;
        } else {
          if (nameA < nameB) return -1;
          if (nameA > nameB) return 1;
        }

        return 0;
      });
      if (!data) res.json("no public emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

module.exports.getMesEmissionEnCours = function(req, res) {
  Emission.find({ status_cloture: req.params.status, emetteur_id : req.params.id })
    .populate("societe_id")
    .exec(function(err, data) {
      if (!data) res.json("no emissions en cours");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};

module.exports.sortByDateEcheanceMes = function(req, res) {
  let ord = "desc";
  if (req.params.order == "true") ord = "desc";
  else ord = "asc";
  console.log(req.params.id)
  Emission.find({ emetteur_id : req.params.id })
    .populate("societe_id")
    .sort({ "emprunt.dateEcheance": ord })
    .exec(function(err, data) {
      if (!data) res.json("no emissions");
      else
      {
        var newData  = data.filter(el => el.emprunt.dateCloture) ;
        res.json(newData);
      } 
    });
};


///  certif de depot 

module.exports.addCertif = function(req, res) {
  var new_certifDepot = new certifDepot(req.body);
  new_certifDepot.save(function(err, certif) {
    if (err) res.send(err);
    else {
      res.json(certif);
    }
  });
};

module.exports.getCertif = function(req, res) {
  certifDepot.find()
  .populate("emetteur_id")
  .exec(function(err, data) {
    if (!data) res.json("no certif");
    res.json(data);
  });
};


///  Billet de tresorerie

module.exports.addBillet = function(req, res) {
  var new_Billet = new BilletTresor(req.body);
  new_Billet.save(function(err, billet) {
    if (err) res.send(err);
    else {
      res.json(billet);
    }
  });
};

module.exports.getBillet= function(req, res) {
  BilletTresor.find()
  .populate("emetteur_id")
  .exec(function(err, data) {
    if (!data) res.json("no billet");
    res.json(data);
  });
};



///  Compte  a terme

module.exports.addCompte= function(req, res) {
  var new_Compte = new CompteTerme(req.body);
  new_Compte.save(function(err, compte) {
    if (err) res.send(err);
    else {
      res.json(compte);
    }
  });
};

module.exports.getCompte= function(req, res) {
  CompteTerme.find()
  .populate("emetteur_id")
  .exec(function(err, data) {
    if (!data) res.json("no Compte a terme");
    res.json(data);
  });
};

