var mongoose = require("mongoose");
var helper = require("../helper/datatable");
var Contact = require("../models/contacts");

module.exports.addContact = function(req, res) {
  var new_contact = new Contact(req.body);
  // new_contact.firstname = "dellaa";
  new_contact.save(function(err, contact) {
    if (err) res.send(err);
    else {
      res.json(contact);
    }
  });
};

module.exports.mesContacts = function(req, res) {
  Contact.find({ user_id: req.body.user_id })
    .lean()
    .exec(function(err, contacts) {
      if (err) return res.status(500).send({ error: err });
      return res.status(200).send(helper(req, contacts));
    });
};

module.exports.inviteContacts = function(req, res) {
  Contact.find({ user_id: req.body.user_id })
    .lean()
    .exec(function(err, contacts) {
      if (err) return res.status(500).send({ error: err });
      return res.status(200).send(contacts);
    });
};
module.exports.updateContact = function(req, res) {
  Contact.findOneAndUpdate({ _id: req.body._id }, req.body)
    .lean()
    .exec(function(err, contact) {
      if (err) res.send(err);
      else {
        res.json(contact);
      }
    });
};

module.exports.deleteContact = function(req, res) {
  console.log(req.body._id)
  Contact.findOneAndDelete({ _id: req.body._id })
    .lean()
    .exec(function(err, contact) {
      if (err) res.send(err);
      else {
        res.json(contact);
      }
    });
};
