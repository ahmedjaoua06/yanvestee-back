var mongoose = require("mongoose");
var Order = require("../models/order");
var Offer = require("../models/offer");
var User = require("../models/user");
module.exports.addOrder = function(req, res) {
  var new_order = new Order();
  new_order.montant = req.body.montant;
  new_order.duree = req.body.duree;
  new_order.investisseur_id = req.body.investisseur_id;
  new_order.date_dispo = req.body.date_dispo;
  new_order.compteAterme = req.body.compteAterme;
  new_order.pensionLivre = req.body.pensionLivre;
  new_order.certificatDeDepot = req.body.certificatDeDepot;
  new_order.billetTresorerie = req.body.billetTresorerie;
  new_order.invisible = req.body.invisible;
  new_order.save(function(err, data) {
    if (err) res.send({ err: err });
    else {
      res.json(data);
    }
  });
};

module.exports.getOrders = function(req, res) {
  Order.find({ isfinished: false })
    .populate("offres")
    .populate({path:"investisseur_id" , select : "firstname lastname logo"})
    .sort({ _id: "desc" })
    .populate({
      path: "offres",
      select: "banque_id order_id taux status create_date compteAterme pensionLivre certificatDeDepot billetTresorerie montant",
      populate: { path: "banque_id" , select : "firstname lastname logo" }
    })
    .lean()
    .exec(function(err, data) {
      data.forEach(element => {
        if (Date.parse(element.date_dispo) < Date.now()) {
          Order.findOneAndUpdate(
            { _id: element._id },
            { isfinished: true },
            function(err, emission) {}
          );
        }
      });
      res.json(data);
    });
};

module.exports.getOrdersById = function(req, res) {
  Order.find({ investisseur_id: req.params.id })
    .sort({ _id: "desc" })
    .populate("offres")
    .populate({path:"investisseur_id" ,select : "firstname lastname logo"})
    .populate({
      path: "offres",
      select: "banque_id order_id taux status create_date compteAterme pensionLivre certificatDeDepot billetTresorerie montant",
      populate: { path: "banque_id" , select : "firstname lastname logo" }
    })
    .lean()
    .exec(function(err, data) {
      data.forEach(element => {
        if (Date.parse(element.date_dispo) < Date.now()) {
          Order.findOneAndUpdate(
            { _id: element._id },
            { isfinished: true },
            function(err, emission) {}
          );
        }
      });
      res.json(data);
    });
};

module.exports.addOffer = function(req, res) {
  var new_offer = new Offer();
  new_offer.taux = req.body.taux;
  new_offer.banque_id = req.body.banque_id;
  new_offer.order_id = req.body.order_id;
  new_offer.compteAterme = req.body.compteAterme;
  new_offer.pensionLivre = req.body.pensionLivre;
  new_offer.certificatDeDepot = req.body.certificatDeDepot;
  new_offer.billetTresorerie = req.body.billetTresorerie;
  new_offer.montant = req.body.montant;
  new_offer.save(function(err, data) {
    if (err) res.send({ err: err });
    else {
      Order.findById(req.body.order_id, function(err, ord) {
        ord.offres.push(data);
        ord.save(function(err, result) {});
      });
      res.json(data);
    }
  });
};

module.exports.decline = function(req, res) {
  console.log(req.body);
  Offer.findOneAndUpdate(
    { _id: req.body.offer_id },
    { status: "Décliner" },
    function(err, d) {
      if (err) res.send({ err: err });
      res.json(d);
    }
  );
};

module.exports.acceptt = function(req, res) {
  console.log(req.body);
  Offer.findOneAndUpdate(
    { _id: req.body.offer_id },
    { status: "Accepter" },
    function(err, d) {
      if (err) res.send({ err: err });

      req.body.decline.forEach(element => {
        Offer.findOneAndUpdate(
          { _id: element },
          { status: "Décliner" },
          function(err, of) {
            if (err) res.send({ err: err });
          }
        );
      });

      Order.findOneAndUpdate(
        { _id: req.body.order_id },
        { isfinished: true },
        function(err, o) {
          if (err) res.send({ err: err });
        }
      );
      res.json(d);
    }
  );
};


module.exports.accepttP = function(req, res) {
  console.log(req.body);
  Offer.findOneAndUpdate(
    { _id: req.body.offer_id },
    { status: "Accepter" , montant : req.body.montant },
    function(err, d) {
      if (err) res.send({ err: err });

      req.body.decline.forEach(element => {
        Offer.findOneAndUpdate(
          { _id: element },
          { status: "Décliner" },
          function(err, of) {
            if (err) res.send({ err: err });
          }
        );
      });

      Order.findOneAndUpdate(
        { _id: req.body.order_id },
        { isfinished: true },
        function(err, o) {
          if (err) res.send({ err: err });
        }
      );
      res.json(d);
    }
  );
};

module.exports.mesOffres = function(req, res) {
  console.log(req.params.id);
  Offer.find({ banque_id: req.params.id })
    .sort({ _id: "desc" })
    .populate("order_id")
    .exec(function(err, data) {
      if (err) res.send({ err: err });
      res.json(data);
    });
};

module.exports.banks = function(req, res) {

  User.find({ categorie: "Banque" })
  .select({ "firstname": 1 , "lastname":1, "_id": 1})
    .exec(function(err, data) {
      if (err) res.send({ err: err });
      res.json(data);
    });
};
