var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ResourceSchema = new Schema({
	name: {type: String},
	absolutePath: {type: String},
	relativePath : {type: String},
	uploadPath : {type : String},
	mimeType: {type: String},
	url: {type: String},
	createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date}
});

module.exports = mongoose.model('Resource', ResourceSchema);