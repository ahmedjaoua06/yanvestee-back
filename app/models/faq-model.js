var mongoose = require("mongoose");

var FAQSchema = new mongoose.Schema({
    question: String,
    answer: String,
    creationDate: { type: Date, default: Date.now },
    updateDate: { type: Date, default: Date.now },
});
module.exports = mongoose.model("FAQ", FAQSchema);