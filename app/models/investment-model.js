var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var InvestmentSchema = new mongoose.Schema({
    investorId: { type: mongoose.Types.ObjectId, ref: 'Users' },
    emissionId: { type: mongoose.Schema.Types.ObjectId, ref: 'Emissions' },
    rate: Number, // 1: FIXED 2: VARIABLE
    amount: Number,
    status: { type: Number, default: 1 }, //1 : PENDING 2: REFUSED 3: ACCEPTED
    creationDate: { type: Date, default: Date.now },
    //IC
    typerate: Number,
    rateIC: Number,
    type: [Number],
    amountPercent: Number,
    offreicID: { type: mongoose.Schema.Types.ObjectId, ref: 'InstitClub' },
    emetteurId: { type: mongoose.Schema.Types.ObjectId, ref: 'Users' },
    updateDate: { type: Date, default: Date.now },
    active:{ type: Boolean, default: true }
});

InvestmentSchema.methods.getCurrentInvestmentsAmounts = function (emissionId) {

}

InvestmentSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Investment', InvestmentSchema);