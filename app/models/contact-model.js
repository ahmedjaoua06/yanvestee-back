const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var contactSchema = mongoose.Schema({
	email:  String,
	firstname: String,
	lastname: String,
	tel : String,
	role : String,
	categorie : String ,
	sector:String,
	categorie:String,
	note:String,
	logo : {
		id: {type: mongoose.Schema.Types.ObjectId, ref: 'Resource'},
		path: String
	},
	societe : String ,
	fonction: String,
    mobile: String,
    tags: [String],
    idUser: {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
	creationDate: {type: Date, default: Date.now},
	updateDate: {type: Date, default: Date.now}
});

contactSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Contact', contactSchema);