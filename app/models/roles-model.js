const mongoose = require('mongoose');

var roleSchema = mongoose.Schema({
	name : String,
	type : String,
	elements : [{
		name : String,
		actions : [{
			name : String,
			able : Boolean
		}]
	}]
});


module.exports = mongoose.model('Roles', roleSchema);