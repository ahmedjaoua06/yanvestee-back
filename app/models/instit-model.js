var mongoose = require("mongoose");
var mongoosePaginate = require('mongoose-paginate');

var InstitSchema = new mongoose.Schema({
    amount: Number,
    type: [Number],
    period: Number,
    availableDate: Date,
    blacklist: [{ type: mongoose.Schema.Types.ObjectId, ref: "Users" }],
    investorId: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    creationDate: { type: Date, default: Date.now },
    updateDate: { type: Date, default: Date.now },
    endEmission: Date,
    active: { type: Boolean, default: true }
});
InstitSchema.plugin(mongoosePaginate);
module.exports = mongoose.model("InstitClub", InstitSchema);