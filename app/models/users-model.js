const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const bcrypt = require('bcrypt');
const saltRounds = 10;
var userSchema = mongoose.Schema({
	email: { type: String, unique: true },
	password: String,
	firstname: String,
	lastname: String,
	tel: String,
	role: String,
	roleId: { type: mongoose.Schema.Types.ObjectId, ref: 'Roles' },
	categorie: String,
	logo: {
		id: { type: mongoose.Schema.Types.ObjectId, ref: 'Resource' },
		path: String
	},
	societe: String,
	fonction: String,
	mobile: String,
	permalink: String,
	passwordToken: String,
	passwordTokenExpirationDate: Date,
	verif: { type: Boolean, default: false },
	contacts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contact' }],
	creationDate: { type: Date, default: Date.now },

	agencenotation: String,
	infoDateNotation: Date,
	ratinglong: String,
	ratingcourt: String,
	perspective: String,
	jort: String,
	infoJuridicForm: String,
	sharecapital: String,
	mf: String,
	lei: String,
	secteur: String,
	adresse: String,
	website: String,
	outstandingAmounts: {
		outstandingEO: Number,
		outstandingBT: Number,
		outstandingCT: Number,
		outstandingCD: Number,
		updateDate: { type: Date, default: Date.now }
	}

});
userSchema.methods.generateHash = function (password) {
	var salt = bcrypt.genSaltSync(saltRounds);
	var hash = bcrypt.hashSync(password, salt);
	return hash;
};

// checking if password is valid
userSchema.methods.validPassword = function (password) {
	return bcrypt.compareSync(password, this.password);
};
userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Users', userSchema);