const mongoose = require('mongoose');
// var mongoosePaginate = require('mongoose-paginate');

var NotifUserSchema = mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    notifId: { type: mongoose.Schema.Types.ObjectId, ref: "Notif" },
    seen: { type: Boolean, default: false },
    creationDate: {type: Date, default: Date.now}
});

// contactSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('NotifUser', NotifUserSchema);