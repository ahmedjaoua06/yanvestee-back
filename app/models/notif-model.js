const mongoose = require('mongoose');
// var mongoosePaginate = require('mongoose-paginate');

var NotifSchema = mongoose.Schema({
    key: String,
    userSender: { type: mongoose.Schema.Types.ObjectId, ref: "Users" },
    creationDate: { type: Date, default: Date.now },
    content: String,
    idEmission: { type: mongoose.Schema.Types.ObjectId, ref: "Emissions" },
});

// contactSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Notif', NotifSchema);