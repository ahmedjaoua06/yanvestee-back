//Models
var NotifModel = require('../models/notif-model');
var NotifUserModel = require('../models/notif-user-model');
var User = require('./../models/users-model');


//Function Create Notification
async function CreateNotification(data, callback) {
    try {
        var notification = new NotifModel(data);
        notification.save((err, notif) => {
            if (err) {
                console.log("===== CreateNotification ====");
                console.log(err);
            }
            else {
                return callback(notification._id);
            }
        });
    } catch (error) {
        console.log(error);
    }
}
//Create notification Users
function CreateNotificationUsers(users, callback) {
    try {
        NotifUserModel.insertMany(users, (err) => {
            if (err) {
                console.log("===== CreateNotificationUsers ====");
                console.log(err);
            }
            else {
                return callback();
            }
        })
    } catch (error) {
        console.log(error)
    }
}
//Exports
//Create Notification
exports.CreateNotification = CreateNotification;
//Create notification Users
exports.CreateUsersNotification = CreateNotificationUsers;
//Get List notification by id User
exports.GetNotificationUserByIdUser = (req, res) => {
    try {
        User.findById(req.decoded.id).exec(function (err, currentUser) {
            console.log(currentUser);
            if (err)
                return res.status(500).json({ message: "user not found" });

            NotifUserModel.find({ userId: currentUser._id }).populate({
                path: 'notifId',
                populate: [{
                    path: 'userSender',
                    select: 'firstname lastname logo'
                }]
            }).limit(5)
                .sort('-creationDate').exec((error, notifs) => {
                    return res.status(200).json({ data: notifs, message: "success" });
                });
        });
    } catch (error) {
        console.log(error);
    }
}
//Change status notif
exports.notifSeenByUser = (req, res) => {
    try {
        User.findById(req.decoded.id).exec(function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: "user not found" });

            NotifUserModel.findByIdAndUpdate({ _id: req.body.idNotif }, { seen: true }, (error, notif) => {
                if (error)
                    return res.status(500).json({ message: "error update" });

                return res.status(200).json({ message: "update successfully" });
            });

        });
    } catch (error) {
        console.log(error);
    }
}
//Get all notifications by id User
exports.getAllNotification = (req, res) => {
    try {
        User.findById(req.decoded.id).exec(function (err, currentUser) {
            console.log(currentUser);
            if (err)
                return res.status(500).json({ message: "user not found" });

            NotifUserModel.find({ userId: currentUser._id }).populate({
                path: 'notifId',
                populate: [{
                    path: 'userSender',
                    select: 'firstname lastname logo'
                }]
            }).sort('-creationDate').exec((error, notifs) => {
                return res.status(200).json({ data: notifs, message: "success" });
            });
        });
    } catch (error) {
        console.log(error);
    }
}