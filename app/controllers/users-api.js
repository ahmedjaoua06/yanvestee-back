var Users = require('./../models/users-model');
var Roles = require('./../models/roles-model');

var Resource = require('./../models/ressource-model');
var nodeMailer = require("nodemailer");
var fs = require('fs');
var jade = require("jade");
var randomstring = require('randomstring');
require('dotenv').config();
//Import Helpers Generate Template
var generateTemplate = require("../helpers/templateMail");
var htmlContact = require("../helpers/templateHTML/html_contact");
//Import Generate password
var generatorPassowrd = require('generate-password');
var serviceHelper = require('../helpers/serviceHelper');
var tempMail = require('../helpers/templateMail');
var rolesDetails = {
	"Emprunt obligataire": {
		id: 'empruntsOb',
		name: "Emprunt obligataire",
		title: 'Emprunt obligataire',
		translate: 'NAV.EMPOB.TITLE',
		type: 'item',
		urlTitle: "/emprunt-obligataire",
		url: ''
	},
	"Certificat de dépôt": {
		id: 'certiDepo',
		name: "Certificat de dépôt",
		title: 'Certificat de dépôt',
		translate: 'NAV.CERTIFDEPO.TITLE',
		type: 'item',
		urlTitle: "/certif",
		url: ''
	},
	"Billets de trésorerie": {
		id: 'commPaper',
		name: "Billets de trésorerie",
		title: 'Billets de trésorerie',
		translate: 'NAV.COMMPAPER.TITLE',
		type: 'item',
		urlTitle: "/billet-tresorie",
		url: ''
	},
	"Comptes à terme": {
		id: 'termAccounts',
		name: "Comptes à terme",
		title: 'Compte à terme',
		translate: 'NAV.TERMACCOUNTS.TITLE',
		type: 'item',
		urlTitle: "/compte-terme",
		url: ''
	}

}
exports.createUser = async (req, res) => {
	var user = req.body.user;
	user.email = user.email.toLowerCase();
	var finalUser = new Users(user);
	finalUser.email = finalUser.email.toLowerCase();
	var checkUser = await Users.findOne({ email: user.email });
	if (checkUser != null) {
		return res.status(401).json({ error: "Cette adresse mail est déjà utilisée" });
	}
	var role = await Roles.findOne({ name: user.categorie, type: user.role });
	if (role == null) {
		return res.status(401).json({ error: "Le rôle n'est pas conforme au spécifications de l'application" });
	} else {
		finalUser.roleId = role._id;
	}
	var htmltoUser;
	var subjetToUser;
	//Check if role is investor


	finalUser.password = finalUser.generateHash(req.body.user.password);
	if (role.type == "Investisseur") {
		htmltoUser = "<p>Bonjour " + user.firstname + ", </p><p>Nous sommes heureux de vous accueillir sur <strong>Yanvestee</strong>.</p><p>Ce compte personnel vous permet d’accéder à la marketplace pour visualiser toutes les offres de nos partenaires et découvrir leurs produits d’épargne et d’investissement. </p><p>Pour accéder à la marketplace dès à présent, validez votre inscription en cliquant sur le lien suivant : <a href='" + process.env.FRONTENDURL + "'>yanvestee.com</a></p><p>Vos identifiants : </p><p><ul><li><strong>Votre adresse email : </strong>" + user.email + "</li><li><strong>Votre mot de passe : </strong>" + req.body.user.password + "</li></ul></p><p>Nous vous recommandons de bien conserver ces informations de connexion. </p><p>Si vous rencontrez un problème pour finaliser votre inscription, n’hésitez pas à nous contacter par mail : helpdesk@yanvestee.com</p><p>A très bientôt,</p><p> L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />';
		subjetToUser = "Yanvestee | Confirmation d’inscription Investisseur";
	} else
	 if (role.type == "Emetteur"){

		htmltoUser = "<p>Bonjour " + user.firstname + " " + user.lastname + ", </p><p>Nous sommes heureux de vous accueillir sur <strong>Yanvestee</strong>.</p><p>Ce compte personnel vous permet d’accéder à l’espace émetteur et bénéficier des fonctionnalités offertes. Vous pouvez également visualiser toutes les offres de nos partenaires et découvrir les produits d’épargne et d'investissement que vous pourrez lister.</p><p>Pour accéder à la marketplace dès à présent, validez votre inscription en cliquant sur le lien suivant : yanvestee.com</p><p>Vos identifiants : </p><p><ul><li><strong>Votre adresse email : </strong>" + user.email + "</li><li><strong>Votre mot de passe : </strong>" + req.body.user.password + ". </li></ul></p><p>Nous vous recommandons de bien conserver ces informations de connexion. </p><p>Si vous rencontrez un problème pour finaliser votre inscription, n’hésitez pas à nous contacter par mail : helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />'
		subjetToUser = "Yanvestee | Confirmation d’inscription Emetteur";
	}

	var template = process.cwd() + "/views/register.jade";
	fs.readFile(template, "utf8", function (err, file) {
		if (err) {
			console.log(err);
			return res.json({ result: "error" + err });
		} else {
			var compiledTmpl = jade.compile(file, { filename: template });
			var context = {
				link: process.env.FRONTENDURL + "/login?verif=" + finalUser._id,
				user: finalUser
			};

			var html = compiledTmpl(context);
			// let transporter = nodeMailer.createTransport({
			// 	service: "gmail",
			// 	auth: {
			// 		user: "inspinia.am@gmail.com",
			// 		pass: "inspinia2019"
			// 	}
			// });

			let mailOptions = {
				from: "inspinia.am@gmail.com",
				to: "inspinia.am@gmail.com",
				subject: "Vérification d'inscription",
				html: html
			};
			tempMail.generateTemplateMail(html, "noreply@yanvestee.com", subjetToUser, () => {
				finalUser.save(function (err) {
					if (role.type == "Investisseur") {
						tempMail.generateTemplateMail(htmltoUser, user.email, subjetToUser, () => {
							return res.status(200).json({ message: finalUser });
						});
					}
					else {
						return res.status(200).json({ message: finalUser });
					}
				});
			});

			// transporter.sendMail(mailOptions, (error, info) => {
			// 	if (error) {
			// 		console.log(error)
			// 	} else {
			// 		console.log("Message Sent!");
			// 		finalUser.save(function (err) {
			// 			if (err) {
			// 				console.log(err);
			// 				return res.status(500).json({ error: err });
			// 			}
			// 			let mailOptions = {
			// 				from: "inspinia.am@gmail.com",
			// 				to: finalUser.email,
			// 				subject: subjetToUser,
			// 				html: htmltoUser
			// 			};
			// 			mailOptions.attachments = [{
			// 				filename: 'Webp.net-resizeimage.png',
			// 				path: './specialFile/Webp.net-resizeimage.png',
			// 				cid: 'unique@kreata.ee' //same cid value as in the html img src
			// 			}];
			// 			transporter.sendMail(mailOptions, (error, info) => {

			// 			})
			// 			return res.status(200).json({ message: finalUser });
			// 		})
			// 	}
			// });
		}
	});
};
exports.confirmUser = (req, res) => {
	Users.findById(req.body.userId, function (err, myUser) {
		if (myUser.verif) {
			return res.status(401).json({ error: "Utilisateur déjà confirmé" })
		}
		myUser.verif = true;
		myUser.save(function (err) {
			if (err) {
				console.log(err);
				return res.status(500).json({ error: err });
			}
			return res.status(200).json({ message: "Utilisateur confirmé" })
		})
	})
}

exports.getProfile = (req, res) => {
	Users.findById(req.decoded.id, function (err, myUser) {
		return res.status(200).json(myUser);
	})
}

exports.updateProfile = (req, res) => {
	Users.findById(req.decoded.id, function (err, myUser) {
		myUser.firstname = req.body.firstname;
		myUser.lastname = req.body.lastname;
		myUser.tel = req.body.tel;
		myUser.societe = req.body.societe;
		myUser.fonction = req.body.fonction;
		myUser.mobile = req.body.mobile;

		myUser.agencenotation = req.body.agencenotation;
		myUser.ratinglong = req.body.ratinglong;
		myUser.ratingcourt = req.body.ratingcourt;
		myUser.perspective = req.body.perspective;
		// myUser.jort = req.body.jort;
		myUser.mf = req.body.mf;
		myUser.lei = req.body.lei;
		myUser.secteur = req.body.secteur;
		myUser.adresse = req.body.adresse;
		myUser.infoJuridicForm = req.body.infoJuridicForm;
		myUser.infoDateNotation = req.body.infoDateNotation;
		myUser.sharecapital = req.body.sharecapital;
		myUser.website = req.body.website;

		myUser.save(function (err, user) {
			if (err) {
				console.log(err);
				return res.status(500).json({ error: err });
			}
			return res.status(200).json({ message: "Profil mis à jour", data: myUser })
		})
	})
}
exports.updatePassword = (req, res) => {
	Users.findById(req.decoded.id, function (err, myUser) {
		myUser.password = myUser.generateHash(req.body.password);;
		myUser.save(function (err) {
			if (err) {
				console.log(err);
				return res.status(500).json({ error: err });
			}
			return res.status(200).json({ message: "Profil mis à jour" })
		})
	})
}

exports.getUserByEmail = (req, res) => {
	Users.findOne({ email: req.body.email }, function (err, myUser) {
		return res.status(200).json(myUser)
	})
}

exports.getMenu = (req, res) => {
	Users.findById(req.decoded.id).populate("roleId").exec(function (err, myUser) {
		if (err) {
			return res.status(500).json(err);
		}
		if (myUser == null) {
			return res.status(401).json({ error: "Aucun utilisateur trouvé" });
		}
		var returnMenu = [];
		var menu = { read: [], write: [] };
		for (var i = 0; i < myUser.roleId.elements.length; i++) {
			var pos = myUser.roleId.elements[i].actions.map(function (e) { return e.name; }).indexOf('Create');
			if (myUser.roleId.elements[i].actions[pos].able) {
				var emissionInput = rolesDetails[myUser.roleId.elements[i].name];
				if (emissionInput != null) {
					var pushInfo = {};
					pushInfo.id = emissionInput.id + "listing";
					pushInfo.name = emissionInput.name;
					pushInfo.title = emissionInput.title;
					pushInfo.translate = emissionInput.translate;
					pushInfo.type = emissionInput.type;
					pushInfo.url = "/listing" + emissionInput.urlTitle;
					menu.write.push(pushInfo);
				}
			}
		}
		//Marketplace
		returnMenu.push({
			id: 'marketPlace',
			title: 'Marketplace',
			translate: 'NAV.LISTING.TITLE',
			type: 'item',
			icon: 'account_balance',
			url: '/marketPlace/marketCards'
		});
		//Deal Club
		returnMenu.push({
			id: 'dealClub',
			title: 'Deal club',
			translate: 'NAV.LISTING.TITLE',
			type: 'item',
			icon: 'swap_horizontal_circle',
			url: '/marketPlace/dealClub'
		});
		//Instit Club
		console.log("Instit Club");
		console.log(myUser.roleId);


		if (myUser.role == "Investisseur" && (myUser.categorie == "Asset Managers" || myUser.categorie == "Assurance")) {
			returnMenu.push({
				id: 'InstitClub',
				title: 'Instit club',
				translate: 'NAV.INSClUB.TITLE',
				type: 'item',
				icon: 'supervised_user_circle',
				url: '/instit-club/list'
			});
		}
		//List Child Listing
		if (menu.write.length > 0) {
			returnMenu.push({
				id: 'listing',
				title: 'Listing',
				translate: 'NAV.LISTING.TITLE',
				type: 'collapsable',
				icon: 'list',
				children: menu.write
			});
		}

		if (myUser.role == "Emetteur") {
			returnMenu.push({
				id: 'InstitClub',
				title: 'Instit club',
				translate: 'NAV.INSClUB.TITLE',
				type: 'item',
				icon: 'supervised_user_circle',
				url: '/instit-club'
			});
		}

		//Contact
		returnMenu.push({
			id: 'mesContacts',
			title: 'Mes contacts',
			type: "item",
			icon: 'account_box',
			url: '/contact'
		});
		//FAQ
		returnMenu.push({
			id: 'faq',
			title: 'FAQ',
			translate: 'NAV.FAQ.TITLE',
			type: 'item',
			icon: 'help_outline',
			url: '/faq'
		});
		return res.status(200).json(returnMenu);
	})
}

exports.sendTokenForgotPassword = (req, res) => {
	try {
		Users.findOne({ email: req.body.email.toLowerCase() }, function (err, myUser) {
			if (myUser != null) {
				var passwordToken = randomstring.generate({
					length: 64
				});
				var permalink = randomstring.generate({
					length: 8
				});
				myUser.permalink = permalink;
				myUser.passwordToken = passwordToken;
				myUser.passwordTokenExpirationDate = Date.now() + 24 * 3600000; // 24 hour
				myUser.save(function (err) {
					if (err) {
						console.log(err);
						return res.status(500).json({ error: err });
					}
					var _html = "<p>Bonjour " + myUser.firstname + "</p><p>Vous avez demandé à réinitialiser le mot de passe pour votre compte Yanvestee.</p><p>Vous pouvez définir un nouveau mot de passe en cliquant sur le lien suivant : <a href='" + process.env.FRONTENDURL + "/reset-password?permalink=" + myUser.permalink + "&token=" + myUser.passwordToken + "'> définir un nouveau mot de passe</a> </p><p>Si vous rencontrez un problème pour finaliser votre inscription, n’hésitez pas à nous contacter par mail : helpdesk@yanvestee.com</p><p>A très bientôt, L'équipe Yanvestee</p>";
					tempMail.generateTemplateMail(_html, myUser.email, "Yanvestee |Modification de votre mot de passe", () => {
						return res.status(200).json({ message: "Email de mot de passe oublié envoyé" });
					});
				})
			} else {
				return res.status(401).json({ error: "Aucun utilisateur trouvé" });
			}
		});
	}
	catch (error) {
		console.log(error)
	}
}

exports.forgotPassword = (req, res) => {
	try {
		Users.findOne({ permalink: req.body.permalink, passwordTokenExpirationDate: { $gt: Date.now() } }, function (err, myUser) {
			if (!myUser) {
				return res.status(401).json({ error: "Token expiré ou non utilisable" });
			}
			if ((req.body.token != myUser.passwordToken) || (req.body.token == undefined)) {
				return res.status(401).json({ error: "Token expiré ou non utilisable" });
			}

			myUser.password = myUser.generateHash(req.body.password);
			myUser.passwordToken = undefined;
			myUser.permalink = undefined;
			myUser.passwordTokenExpirationDate = undefined;
			myUser.save(function (err) {
				if (err) {
					console.log(err);
					return res.status(500).json({ error: err });
				}
				return res.status(200).json({ message: "Le mot de passe a été mis a jour" });
			})
		});
	}
	catch (error) {
		logger.error(error)
	}
}

//Upload File User Profile
exports.uploadFileUserProfile = (req, res) => {
	try {
		//Get Current User
		Users.findById(req.decoded.id, async function (err, currentUser) {
			//Check if current user has logo
			if (currentUser.logo.id != undefined) {
				console.log("Remove File from database");
				//Remove File from database
				Resource.remove({ _id: currentUser.logo.id }, function (err) {
					if (!err) {
						console.log("Remove file from database successfully");
					}
				});
				//Remove File from disk
				var pathFile = currentUser.logo.path.replace("/static", "./public");
				fs.unlink(pathFile, function (error) {
					if (!error) {
						console.log("Remove file from disk successfully");
					}
				});
			}
			//Save File Upload
			serviceHelper.uploadFile(req.body.image, req.body.fileName, function (filePath, fileName, fileType) {
				var currentDate = new Date();
				var newResource = new Resource();
				newResource.relativePath = filePath;
				newResource.name = req.body.fileName;
				newResource.mimeType = fileType;
				newResource.uploadPath = '/static/resources/' + req.body.fileName;
				newResource.creationDate = currentDate;
				newResource.save(function (err, resou) {
					currentUser.logo.id = resou.id;
					currentUser.logo.path = newResource.uploadPath;
					currentUser.updateDate = currentDate;
					currentUser.save(function (err) {
						if (err) {
							console.log(err);
							return res.status(500).json(err);
						}
						return res.status(200).json({ message: "Logo added" });
					})
				});
				console.log("Hola");
			});
		});
	} catch (error) {
		console.log(error);
	}
}

//Update outstanding
exports.updateOutStanding = (req, res) => {

	try {
		Users.findById(req.decoded.id, async function (err, currentUser) {
			if (err)
				return res.status(500).json({ error: err });

			//Outstanding Amounts
			currentUser.outstandingAmounts = req.body.outstandingAmounts;
			currentUser.outstandingAmounts.updateDate = new Date();

			currentUser.save(function (err, user) {
				console.log(err);
				console.log(user);
				if (err)
					return res.status(500).json({ error: err });
				return res.status(200).json({ data: user })
			});

		});
	} catch (error) {
		console.log(error);
	}

}