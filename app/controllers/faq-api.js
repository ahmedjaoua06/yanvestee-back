var FaqModel = require('../models/faq-model');
var serviceHelper = require('../helpers/serviceHelper');
//Get All FAQ
exports.getAllFAQ = (req, res) => {
    try {
        FaqModel.find({}).exec((err, faqs) => {
            if (!err)
                return res.status(200).json({ message: "success", data: faqs });

            return res.status(500).json({ message: err });

        });
    } catch (error) {
        console.log(error);
    }
}
//Init list faq
exports.initListFaq = () => {

    //Delet FAQ Collection
    FaqModel.remove({}, (err) => {
        if (err)
            console.log(err);

        FaqModel.insertMany(serviceHelper.getListFAQ, (errInsert) => {
            if (errInsert)
                console.log("Error")

            console.log("Success")
        });
    });

}