var Investment = require("./../models/investment-model");
var User = require('./../models/users-model');
var Emission = require('./../models/emission-model');
var generateTemplate = require("../helpers/templateMail");
var NotifControler = require('../controllers/notif-api');

exports.createInvestment = function (req, res, callback) {
  User.findById(req.decoded.id).exec(function (err, myUser) {

    var investment = req.body.investment;

    //BT CT CD
    if (req.body.type != "Emprunt obligataire") {
      Emission.findById(req.body.investment.emissionId).populate("emetteurId").exec(function (err, myEmission) {

        investment.investorId = req.decoded.id;
        var finalInvestment = new Investment(investment);
        generateTemplate.generateTemplateMail("<p>Bonjour " + myUser.firstname + ", </p><p>Vous avez déposé une demande de souscription d’un montant de " + investment.amount + " Dinars pour une emission du type " + myEmission.type + " proposé par " + myEmission.emetteurId.societe + ".</p><p>Votre demande sera traitée sous 24 heures.</p><p>Si vous ne recevez aucune notification, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', myUser.email, "Yanvestee | Votre Demande de souscription ", async function () {
          generateTemplate.generateTemplateMail("<p>Bonjour " + myEmission.emetteurId.firstname + ", </p><p>Vous avez reçu une nouvelle demande de souscription d’un montant de " + req.body.amount + " Dinars pour un " + myEmission.type + " proposé par " + myUser.societe + ".</p><p>Votre demande sera traitée sous 24 heures.</p><p>Si vous ne recevez aucune notification, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', myEmission.emetteurId.email, "Yanvestee | Votre Demande de souscription ", async function () {

            var dataNotif = {
              key: "CREATE_INV",
              userSender: req.decoded.id,
              idEmission: finalInvestment.emissionId,
              content: 'a souscrit d’un montant de ' + finalInvestment.amount
            }

            finalInvestment.save(function (err) {
              if (err) {
                console.log(err);
                return res.status(500).json({ error: err });
              }


              if (dataNotif) {
                //Add Notification
                NotifControler.CreateNotification(dataNotif, (notifID) => {
                  var listUser = [{ notifId: notifID, userId: myEmission.emetteurId }]
                  //Add notifications users
                  NotifControler.CreateUsersNotification(listUser, () => {
                    return res.status(200).json({ message: finalInvestment });
                  })
                });
              }
            });
          })
        });
      });
    }
    //EO
    else {
      var docs = [];
      var items = investment.filter(el => el.length > 0).
        map(a => a.
          map(el =>
            new Object({ emissionId: el.idEmission, investorId: myUser._id, amount: el.amount, rate: el.rate })));

      items.forEach((item) => {
        item.forEach((el) => {
          docs.push(el);
        });
      });
      console.log("=== EO Docs ===");
      console.log(docs);
      Emission.findById(docs[0].emissionId).populate("emetteurId").exec(function (err, myEmission) {

        Investment.insertMany(docs, (err) => {
          if (err) {
            return res.status(500).json({ error: err });
          }
          //Loop form all investments
          docs.forEach((item) => {
            var dataNotif = {
              key: "CREATE_INV",
              userSender: req.decoded.id,
              idEmission: myEmission._id,
              content: 'a souscrit d’un montant de ' + item.amount
            }
            NotifControler.CreateNotification(dataNotif, (notifID) => {
              var listUser = [{ notifId: notifID, userId: myEmission.emetteurId._id }]
              //Add notifications users
              NotifControler.CreateUsersNotification(listUser, () => {
                //Send Notification Email To Investor
                generateTemplate.generateTemplateMail("<p>Bonjour " + myUser.firstname + ", </p><p>Vous avez déposé une demande de souscription d’un montant de " + item.amount + " Dinars pour une emission du type emprunt obligataire proposé par " + myEmission.emetteurId.societe + ".</p><p>Votre demande sera traitée sous 24 heures.</p><p>Si vous ne recevez aucune notification, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', myUser.email, "Yanvestee | Votre Demande de souscription ", async function () {
                  //Send Notification Email To Owner Offer
                  generateTemplate.generateTemplateMail("<p>Bonjour " + myEmission.emetteurId.firstname + ", </p><p>Vous avez reçu une nouvelle demande de souscription d’un montant de " + item.amount + " Dinars pour une emission du type emprunt obligataire proposé par " + myUser.societe + ".</p><p>Votre demande sera traitée sous 24 heures.</p><p>Si vous ne recevez aucune notification, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', myEmission.emetteurId.email, "Yanvestee | Votre Demande de souscription ", async function () {
                  });
                });
              });
            });
          });




          return res.status(200).json({ data: "success" });
        });
      });
    }
  });
};

exports.updateInvestment = (req, res) => {
  Investment.findById(req.body.investissementId).exec((err, myinvestment) => {
    Emission.findById(myinvestment.emissionId).populate('emetteurId').exec((errEmission, emission) => {

      myinvestment.status = req.body.status;
      var responseOffer = "";
      var subjectMail = "";
      var dataNotif = {
        key: "ACCEPTORREFUSE_INV",
        userSender: req.decoded.id,
        idEmission: myinvestment.emissionId,
      }

      if (req.body.status == 3) {
        dataNotif.content = "a accepté votre souscription";
        responseOffer = "acceptée";
        subjectMail = "Acceptation de votre demande de souscription";
      }
      else if (req.body.status == 2) {
        dataNotif.content = "a refusé votre souscription";
        responseOffer = "déclinée";
        subjectMail = "Refus de votre demande de souscription";
      }


      myinvestment.updateDate = new Date();
      myinvestment.save(err => {
        if (err) {
          console.log(err);
          return res.status(500).json({ error: err });
        }



        if (dataNotif) {
          //Add Notification
          NotifControler.CreateNotification(dataNotif, (notifID) => {
            var listUser = [{ notifId: notifID, userId: myinvestment.investorId }]
            //Add notifications users
            NotifControler.CreateUsersNotification(listUser, () => {
              User.findById(myinvestment.investorId).exec((errUser, investor) => {

                var htmlTemplate = `
            <p>Bonjour ${investor.societe},</p> 
            <p>Votre nouvelle demande de souscription d’un montant de ${myinvestment.amount} proposé par ${emission.emetteurId.societe} a été ${responseOffer}.</p>
            <p>Un conseiller prendra en charge votre dossier.</p>
            <p>Pour toute autre question, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.coms</p>
            <p>A très bientôt,</p> 
            <p>L'équipe Yanvestee</p>
            `;
                generateTemplate.generateTemplateMail(htmlTemplate, investor.email, subjectMail, () => {
                  return res.status(200).json({ message: "Investissement mis à jour" });
                });
              });
            })
          });
        }

      });
    });
  });
};
exports.investmentList = (req, res) => {
  User.findById(req.decoded.id).exec(async function (err, myUser) {
    if (err) return res.status(500).json({ message: err });

    var emission = await Emission.findById(req.body.emissionId);
    if (emission.emetteurId.equals(myUser._id) || emission.conseillerId.equals(myUser._id)) {

      if (req.body.listEmission.length > 0) {
        var querry = { emissionId: { $in: req.body.listEmission } };
      }
      else {
        var querry = { emissionId: { $eq: req.body.emissionId } };
      }

      //Set page
      var page =
        req.body.pageIndex != undefined ? parseInt(req.body.pageIndex) : 1;

      //Set Limitpage
      var limitPage =
        req.body.limitPage != undefined ? parseInt(req.body.limitPage) : 10;

      //Set SortType
      if (req.body.sortType != undefined) {
        var sortObject = {};
        var stype = req.body.sortType;
        var sdir = req.body.sortOrder;
        sortObject[stype] = sdir;
      } else {
        var sortObject = { creationDate: -1 };
      }

      var options = {
        sort: sortObject,
        populate: "investorId",
        lean: true,
        page: page,
        limit: limitPage
      };

      Investment.paginate(querry, options).then(function (result) {
        var somme = 0;
        var nbinvestment = 0;
        result.docs.forEach(doc => {
          if (doc.status == 3) {
            somme += doc.amount; nbinvestment += 1;
          }
        });
        return res.status(200).json({ message: "success", data: result, somme: somme, nbinvestment: nbinvestment });
      });
    } else {
      //if type emission is BT CD CAT
      if (req.body.listEmission.length > 0) {
        //Get All Investments by list emission id
        var allInvestementsByEmissionId = await Investment.find({ emissionId: { $in: req.body.listEmission } });
        //Get All Investments by current user
        var investmentByUser = await Investment.find({ emissionId: { $in: req.body.listEmission }, investorId: myUser._id });
        //Sum amounts
        var somme = 0;
        //Nbr investments
        var nbinvestment = 0;
        allInvestementsByEmissionId.forEach(el => {
          if (el.status == 3) {
            somme += el.amount; nbinvestment += 1;
          }
        });
        res.status(200).json({ message: "not emission owner", disable: true, investment: investmentByUser, somme: somme, nbinvestment: nbinvestment });
      }
      else {
        //Get All Investments by emission id
        var allInvestementsByEmissionId = await Investment.find({ emissionId: req.body.emissionId });
        //Get All Investments by current user
        var investmentByUser = await Investment.find({ emissionId: req.body.emissionId, investorId: myUser._id });
        //Sum amounts
        var somme = 0;
        //Nbr investments
        var nbinvestment = 0;
        allInvestementsByEmissionId.forEach(el => {
          if (el.status == 3) {
            somme += el.amount; nbinvestment += 1;
          }
        });
        res.status(200).json({ message: "not emission owner", disable: true, investment: investmentByUser, somme: somme, nbinvestment: nbinvestment });
      }
    }
  });
};
exports.myinvestmentsInvestor = (req, res) => {
  try {
    //Get Current User
    User.findById(req.decoded.id).exec(async function (err, currentUser) {

      if (err)
        return res.status(500).json({ message: err });
      if (req.body.listEmission.length > 0) {
        var querry = { investorId: { $eq: currentUser._id }, emissionId: { $in: req.body.listEmission } };
      }
      else {
        //Get list investments by current user
        var querry = { investorId: { $eq: currentUser._id }, emissionId: { $eq: req.body.emissionId } };
      }

      //Set page
      var page =
        req.body.pageIndex != undefined ? parseInt(req.body.pageIndex) : 1;

      //Set Limitpage
      var limitPage =
        req.body.limitPage != undefined ? parseInt(req.body.limitPage) : 10;

      //Set SortType
      if (req.body.sortType != undefined) {
        var sortObject = {};
        var stype = req.body.sortType;
        var sdir = req.body.sortOrder;
        sortObject[stype] = sdir;
      } else {
        var sortObject = { creationDate: -1 };
      }

      var options = {
        sort: sortObject,
        lean: true,
        page: page,
        limit: limitPage
      };

      Investment.paginate(querry, options).then(function (investments) {
        return res.status(200).json({ message: "success", data: investments });
      });
    });
  } catch (error) {
    console.log("myinvestmentsInvestor");
    console.log(error);
  }
}

currentInvestmentAmount = (req, res) => {
  Investment.aggregate();
};