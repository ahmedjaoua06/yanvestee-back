var Investment = require("./../models/investment-model");
var User = require('../models/users-model');
var Emission = require('../models/emission-model');
var mailer = require('../helpers/templateMail')

exports.email = function(req, res, callback) {
  User.findById(req.decoded.id).exec(async function(err, myUser) {
    if (err) return res.status(500).json({ message: err });
    var emission = await Emission.findById(req.body.emissionId);
    if (emission.emetteurId.equals(myUser._id)) {
      Investment.findById(req.body.investissementId).populate("emissionId").exec((err, myinvestment) => {
        User.findById(myinvestment.investorId).exec(async function(err, investor) {
          if(req.body.state){
            mailer.generateTemplateMail( "<p>Bonjour "+ investor.firstname+", </p><p>Votre nouvelle demande de souscription d’un montant de "+ myinvestment.amount+" pour un "+ myinvestment.emissionId.type +" proposé par "+ myUser.societe +" a été acceptée.</p><p>Un conseiller prendra en charge votre dossier.</p><p>Pour toute autre question, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>"+'<td><img src="cid:'+ 'unique@kreata.ee'  +'"/></td><br />', investor.email, "Yanvestee | Votre Demande de souscription" , function () {
            return res.status(200).json({ message: "l'investissement a été accepté avec succès"});
          });
          } else {
            mailer.generateTemplateMail( "<p>Bonjour "+ investor.firstname+", </p><p>Votre nouvelle demande de souscription d’un montant de "+ myinvestment.amount+" pour un "+ myinvestment.emissionId.type +" proposé par "+ myUser.societe +" a été refusée.</p><p>Pour toute autre question, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>"+'<td><img src="cid:'+ 'unique@kreata.ee'  +'"/></td><br />', investor.email, "Yanvestee | Votre Demande de souscription" , function () {
            return res.status(200).json({ message: "l'investissement a été refusée avec succès"});
          });
          }
          
        });
      });    
    }
  })
};
