require('dotenv').config();
var mongoose = require("mongoose");
var Emission = require('./../models/emission-model');
var User = require('./../models/users-model');
var Role = require('./../models/roles-model');
var Contact = require('./../models/contact-model');

var nodeMailer = require("nodemailer");
var fs = require('fs');
var jade = require("jade");
var randomstring = require('randomstring');
require('dotenv').config();
var generateTemplate = require("../helpers/templateMail");
var generatorPassowrd = require('generate-password');
var htmlContact = require("../helpers/templateHTML/html_contact");
var serviceHelper = require('../helpers/serviceHelper');
var Resource = require('./../models/ressource-model');
var offreContactMail = require('../helpers/templateHTML/html_offreContact');
var offreContactandOfferMail = require('../helpers/templateHTML/html_offreContactandOffer');
var contactEmailTemplate = require('../helpers/templateHTML/html_contact_email');
var Investment = require("./../models/investment-model");
var _ = require('lodash');
var NotifControler = require('../controllers/notif-api');
//moment
var moment = require('moment');
moment().format();
async function getInvestmentSome() {
	var investements = await Investment.find({});
	var somme = 0;
	var nbinvestment = 0;
	investements.forEach(investment => { if (investment.status == 3) somme += investment.amount; nbinvestment += 1; });
	return somme;
}
var createEmission = async function (req, res, type, callback) {
	User.findById(req.decoded.id).exec((errUser, currentUser) => {

		var emission = req.body.emission;
		emission.type = type;
		emission.emetteurId = req.decoded.id;
		//Set end date
		emission.endEmission = moment().add(3, 'M');
		var finalEmission = new Emission(emission);
		finalEmission.save(async function (err) {
			if (err) {
				console.log(err);
				return res.status(500).json({ error: err });
			}
			//Check Type Emission
			var dataNotif = {
				key: "CREATE_",
				userSender: req.decoded.id,
				idEmission: finalEmission._id
			}
			switch (type) {
				case 'Certificat de dépôt':
					dataNotif.key += "CD";
					dataNotif.content = " a publié une offre CD";
					break;
				case 'Comptes à terme':
					dataNotif.key += "CT";
					dataNotif.content = " a publié une offre CT";
					break;
				case 'Billets de trésorerie':
					dataNotif.key += "BT";
					dataNotif.content = " a publié une offre BT";
					break;
				default:
					break;
			}
			var listUsers = await User.aggregate([
				{ $match: { $or: [{ role: 'Investisseur' }, { categorie: { $in: ['Banque', 'Leasing, factoring'] } }] } },
				{
					$project: {
						_id: 0,
						'userId': '$_id'
					}
				}
			]);
			listUsers = listUsers.filter(a => a.userId != req.decoded.id);


			var idsUsers = listUsers.map(a => a.userId);
			var _users = await User.find({ _id: { $in: idsUsers } });

			_users.forEach((el) => {
				generateTemplate.generateTemplateMail("<p>Bonjour " + el.societe + "</p><p> " + currentUser.societe + " a publié une nouvelle offre de " + type + " sur Yanvestee Marketplace et vous invite à la consulter.</p><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + finalEmission._id + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', el.email, "Invitation investisseurs", async function () {
				});
			});
			if (dataNotif) {
				console.log("=== Data Notif ===");
				console.log(dataNotif);

				console.log("=== listUsers ===");
				console.log(listUsers);
				//Add Notification
				NotifControler.CreateNotification(dataNotif, (notifID) => {
					listUsers.map(a => a.notifId = notifID);
					//Add notifications users
					NotifControler.CreateUsersNotification(listUsers, () => {
						return res.status(200).json({ message: finalEmission });
					})
				});
			}
		});
	});
};

exports.emissionList = (req, res) => {
	var currentDate = moment();
	var querry = { $and: [{ $or: [{ emetteurId: req.decoded.id }, { conseillerId: req.decoded.id }] }, { type: req.body.type }] };

	//Check if filter by type exist
	if (req.body.status != undefined) {
		req.body.status == 1 ? querry.$and.push({ endEmission: { $gte: currentDate }, active: true }) : querry.$and.push({ $or: [{ endEmission: { $lte: currentDate } }, { active: false }] });
	}
	if (req.body.pageIndex != undefined) {
		var page = parseInt(req.body.pageIndex);
	} else {
		var page = 1;
	}
	if (req.body.limitPage != undefined) {
		var limitPage = parseInt(req.body.limitPage);
	} else {
		var limitPage = 10;
	}
	if (req.body.sortType != undefined) {
		var sortObject = {};
		var stype = req.body.sortType;
		var sdir = req.body.sortOrder;
		sortObject[stype] = sdir;
	} else {
		var sortObject = { creationDate: -1 };
	}

	var options = {
		sort: sortObject,
		lean: true,
		page: page,
		limit: limitPage
	};

	Emission.paginate(querry, options).then(function (result) {
		return res.status(200).json(result)
	});
}
//Export Send Mail to contact
exports.sendMailToContact = (req, res) => {
	try {
		User.findById(req.decoded.id).populate('roleId').exec(function (err, currentUser) {
			if (err)
				return res.status(500).json(err)
			var fullname = currentUser.lastname + " " + currentUser.firstname;
			generateTemplate.generateTemplateMail(contactEmailTemplate.generateMailContact(req.body.message, fullname), req.body.email, currentUser.firstname + " vous a envoyé un nouveau message ", async function () {
				return res.status(200).json({ message: "success" });
			});
		});
	} catch (error) {
		console.log(error);
	}
}
exports.emissionMarket = (req, res) => {
	User.findById(req.decoded.id).populate('roleId').exec(function (err, myUser) {
		var canSeeEmissions = [];
		for (var i = 0; i < myUser.roleId.elements.length; i++) {
			var pos = myUser.roleId.elements[i].actions.map(function (e) { return e.name; }).indexOf('Read');
			if (myUser.roleId.elements[i].actions[pos].able) {
				canSeeEmissions.push(myUser.roleId.elements[i].name)
			}
		}
		if ((req.body.type != undefined) && (req.body.type != 'Tout')) {
			var querry = { active: true, $and: [{ private: { $ne: true } }, { type: req.body.type }] };
		} else {
			var querry = { active: true, $and: [{ private: { $ne: true } }, { $or: [{}, {}] }] };
			for (var i = 0; i < canSeeEmissions.length; i++) {
				querry.$and[1].$or.push({ type: canSeeEmissions[i] })
			}
		}
		if ((req.body.duree != undefined) && (req.body.duree != -1)) {
			querry.$and.push({ period: req.body.duree })
		}
		if (req.body.pageIndex != undefined) {
			var page = parseInt(req.body.pageIndex);
		} else {
			var page = 1;
		}
		if (req.body.limitPage != undefined) {
			var limitPage = parseInt(req.body.limitPage);
		} else {
			var limitPage = 30;
		}

		//Type market is my deals
		if (req.body.typeMarket) {
			querry.$and.push({ emetteurId: myUser._id });
		}
		if (req.body.sortType != undefined) {
			var sortObject = {};
			var stype = req.body.sortType;
			var sdir = req.body.sortOrder;
			sortObject[stype] = sdir;
		} else {
			var sortObject = { creationDate: 1 };
		}

		var options = {
			sort: sortObject,
			populate: "emetteurId",
			lean: true,
			page: page,
			limit: limitPage
		};

		Emission.paginate(querry, options).then(function (result) {
			return res.status(200).json(result)
		});
	})
}

exports.emissionByPeriod = (req, res) => {
	User.findById(req.decoded.id).populate('roleId').exec(function (err, myUser) {
		var canSeeEmissions = [];
		if (req.body.searchType == "market") {
			for (var i = 0; i < myUser.roleId.elements.length; i++) {
				var pos = myUser.roleId.elements[i].actions.map(function (e) { return e.name; }).indexOf('Read');
				if (myUser.roleId.elements[i].actions[pos].able) {
					canSeeEmissions.push(myUser.roleId.elements[i].name)
				}
			}
			if ((req.body.type != undefined) && (req.body.type != 'Tout')) {
				var querry = { active: true, $and: [{ private: { $ne: true } }, { type: req.body.type }] };
			} else {
				var querry = { active: true, $and: [{ private: { $ne: true } }, { $or: [{}, {}] }] };
				for (var i = 0; i < canSeeEmissions.length; i++) {
					querry.$and[1].$or.push({ type: canSeeEmissions[i] })
				}
			}
		} else {
			for (var i = 0; i < myUser.roleId.elements.length; i++) {
				var pos = myUser.roleId.elements[i].actions.map(function (e) { return e.name; }).indexOf('Read');
				if (myUser.roleId.elements[i].actions[pos].able) {
					canSeeEmissions.push(myUser.roleId.elements[i].name)
				}
			}
			if ((req.body.type != undefined) && (req.body.type != 'Tout')) {
				var querry = { $and: [{ private: true }, { type: req.body.type }] };
			} else {
				var querry = { $and: [{ private: true }, { $or: [{}, {}] }] };
				for (var i = 0; i < canSeeEmissions.length; i++) {
					querry.$and[1].$or.push({ type: canSeeEmissions[i] })
				}
			}
		}

		if ((req.body.duree != undefined) && (req.body.duree != -1)) {
			querry.$and.push({ period: req.body.duree })
		}

		var periods = { "Court terme": { "3": 0, "6": 0, "9": 0 }, "Long terme": { "12": 0, "24": 0, "36": 0, "48": 0, "60": 0 } };

		Emission.find(querry).sort("period").then(function (result) {
			/*const officersIds = result.map(officer => officer.period);*/
			var groupByperiod = result.reduce((acc, it) => {
				acc[it.period] = acc[it.period] + 1 || 1;
				return acc;
			}, {});
			for (var item in periods) {
				for (var property1 in periods[item]) {
					if (groupByperiod[property1]) {
						periods[item][property1] = groupByperiod[property1];
					}
				}
			}
			return res.status(200).json(periods)
		});
	})
}

exports.dealClub = (req, res) => {
	User.findById(req.decoded.id).populate('roleId').exec(function (err, myUser) {
		var canSeeEmissions = [];
		for (var i = 0; i < myUser.roleId.elements.length; i++) {
			var pos = myUser.roleId.elements[i].actions.map(function (e) { return e.name; }).indexOf('Read');
			if (myUser.roleId.elements[i].actions[pos].able) {
				canSeeEmissions.push(myUser.roleId.elements[i].name)
			}
		}
		if ((req.body.type != undefined) && (req.body.type != 'Tout')) {
			var querry = { $and: [{ private: true }, { type: req.body.type }] };
		} else {
			var querry = { $and: [{ private: true }, { $or: [{}, {}] }] };
			for (var i = 0; i < canSeeEmissions.length; i++) {
				querry.$and[1].$or.push({ type: canSeeEmissions[i] })
			}
		}
		if ((req.body.duree != undefined) && (req.body.duree != -1)) {
			querry.$and.push({ period: req.body.duree })
		}
		querry.$and.push({ $or: [{ emetteurId: req.decoded.id }, { accesEmission: req.decoded.id }] })
		if (req.body.pageIndex != undefined) {
			var page = parseInt(req.body.pageIndex);
		} else {
			var page = 1;
		}
		if (req.body.limitPage != undefined) {
			var limitPage = parseInt(req.body.limitPage);
		} else {
			var limitPage = 30;
		}
		if (req.body.sortType != undefined) {
			var sortObject = {};
			var stype = req.body.sortType;
			var sdir = req.body.sortOrder;
			sortObject[stype] = sdir;
		} else {
			var sortObject = { creationDate: 1 };
		}

		var options = {
			sort: sortObject,
			populate: "emetteurId",
			lean: true,
			page: page,
			limit: limitPage
		};

		Emission.paginate(querry, options).then(function (result) {
			return res.status(200).json(result)
		});
	})
}

exports.getEmission = async (req, res) => {
	try {

		var emission = await Emission.findById(req.params.emissionId).populate("emetteurId noteOperation documentJuridique documentComptable");
		if (emission) {
			//Check if type emission is EO
			if (emission.type == 'Emprunt obligataire') {
				var emissionEOByParent = await Emission.find({ parentID: emission.parentID }).populate("emetteurId noteOperation documentJuridique documentComptable");
				if (!emissionEOByParent)
					return res.status(401).json({ error: "Aucune émission a afficher" });

				//Set first value

				var result = JSON.parse(JSON.stringify(emissionEOByParent[0]));
				//Add new property children
				result.children = [];
				//Loop in array emissions
				emissionEOByParent.forEach((el) => {
					var objChildren = {};
					objChildren.idEmission = el._id;
					objChildren.category = el.category;
					objChildren.RembourseMaturity = el.RembourseMaturity;
					objChildren.period = el.period;
					objChildren.RembourseModaliteAmortie = el.RembourseModaliteAmortie;
					objChildren.RembourseModaliteInFine = el.RembourseModaliteInFine;
					objChildren.RembourseFrequenceCouponYear = el.RembourseFrequenceCouponYear;
					objChildren.RembourseFrequenceCouponSemester = el.RembourseFrequenceCouponSemester;
					objChildren.RembourseYearGraceBool = el.RembourseYearGraceBool;
					objChildren.RembourseYearGraceAmount = el.RembourseYearGraceAmount;
					objChildren.rateFixedNominal = el.rateFixedNominal;
					objChildren.RembourseTauxFixeBool = el.RembourseTauxFixeBool;
					objChildren.RembourseTauxFixeAmount = el.RembourseTauxFixeAmount;
					objChildren.RembourseTauxVariableBool = el.RembourseTauxVariableBool;
					objChildren.RembourseTauxVariableAmount = el.RembourseTauxVariableAmount;
					result.children.push(objChildren);
				});
				//Sort children by category
				result.children.sort((a, b) => (a.category < b.category) ? -1 : 1);
				console.log("==== Children =====");
				result.children = _.groupBy(result.children, 'category');
				console.log(result.children);
				return res.status(200).json(result);
			}
			//Emission type is BT CD CAT
			return res.status(200).json(emission);
		}
		return res.status(401).json({ error: "Aucune émission a afficher" });
	} catch (error) {
		console.log(error);
	}
}

exports.createCD = (req, res) => {
	createEmission(req, res, "Certificat de dépôt")
}

exports.createEO = async (req, res) => {
	var EmissionEo = {};
	EmissionEo.emetteurId = req.decoded.id;
	EmissionEo.type = "Emprunt obligataire";
	EmissionEo.parentID = new mongoose.mongo.ObjectId();
	EmissionEo.infoRaisonSocial = req.body.info.RaisonSocial;
	EmissionEo.infoSecteur = req.body.info.Secteur;
	EmissionEo.infoJuridicForm = req.body.info.JuridicForm;
	EmissionEo.infoAdresse = req.body.info.Adresse;
	EmissionEo.infoSiteWeb = req.body.info.SiteWeb;
	EmissionEo.infoRating = req.body.info.Rating;
	EmissionEo.miniInvest = req.body.emprunt.miniInvest;

	if (req.body.info.Rating) {
		EmissionEo.infoAgenceDeNotation = req.body.info.AgenceDeNotation;
		EmissionEo.infoRatingLong = req.body.info.RatingLong;
		EmissionEo.infoRatingCourt = req.body.info.RatingCourt;
		EmissionEo.infoDateNotation = req.body.info.DateNotation;
	}
	EmissionEo.infoStatusObligatoire = req.body.info.StatusObligatoire;
	if (req.body.info.StatusObligatoire) {
		EmissionEo.infoDateDecisionAGO = req.body.info.DateDecisionAGO;
		EmissionEo.infoDateConseilAdd = req.body.info.DateConseilAdd;
	}

	EmissionEo.EmpruntName = req.body.emprunt.nameEmprunt;
	EmissionEo.EmpruntMontant = req.body.emprunt.Montant;
	// EmissionEo.miniInvest = req.body.emprunt.Montant;
	EmissionEo.EmpruntMontantModifiableBool = req.body.emprunt.EmpruntModifiable;
	if (req.body.emprunt.EmpruntModifiable) {
		EmissionEo.EmpruntMontantModifiable = req.body.emprunt.MontantModifiable;
	}
	EmissionEo.EmpruntPrixEmission = req.body.emprunt.PrixEmission;
	EmissionEo.EmpruntDateDemarrage = req.body.emprunt.DateDemmarage;
	EmissionEo.EmpruntDatecloture = req.body.emprunt.DateCloture;
	EmissionEo.EmpruntDateJouissance = req.body.emprunt.DateJouissance;
	EmissionEo.EmpruntGarantie = req.body.emprunt.Garantie;
	EmissionEo.EmpruntRating = req.body.emprunt.SecondRating;
	if (req.body.emprunt.SecondRating) {
		EmissionEo.EmpruntAgenceNotation = req.body.emprunt.AgenceNotation;
		EmissionEo.EmpruntDateRating = req.body.emprunt.DateRating;
		EmissionEo.EmpruntNotationEmprunt = req.body.emprunt.NotationEmprunt;
		EmissionEo.EmpruntRatingCourt = req.body.emprunt.SecondRatingCourt;
	}
	EmissionEo.EmpruntAppelPublicBool = req.body.emprunt.AppelPublic;
	if (req.body.emprunt.AppelPublic) {
		EmissionEo.EmpruntVisaCMF = req.body.emprunt.VisaCMF;
		EmissionEo.EmpruntJort = req.body.emprunt.Jort;
		EmissionEo.EmpruntDateVisaCMF = req.body.emprunt.DataVisa;
		EmissionEo.EmpruntDateJort = req.body.emprunt.DateJort;
	}

	EmissionEo.emtteurSociete = req.body.emetteur.EmetteurSociete;
	EmissionEo.emtteurNom = req.body.emetteur.EmetteurNom;
	EmissionEo.emtteurPrenom = req.body.emetteur.EmetteurPrenom;
	EmissionEo.emtteurPhone = req.body.emetteur.EmetteurPhone;
	EmissionEo.emtteurEmail = req.body.emetteur.EmetteurEmail;
	EmissionEo.emtteurSiteWeb = req.body.emetteur.EmetteurSite;
	EmissionEo.conseillerBool = req.body.emetteur.AddConseiller;
	if (req.body.emetteur.AddConseiller) {
		EmissionEo.conseillerSociete = req.body.emetteur.ConseillerSociete;
		EmissionEo.conseillerNom = req.body.emetteur.ConseillerNom;
		EmissionEo.conseillerPrenom = req.body.emetteur.ConseillerPrenom;
		EmissionEo.conseillerPhone = req.body.emetteur.ConseillerPhone;
		EmissionEo.conseillerEmail = req.body.emetteur.ConseillerEmail;
		EmissionEo.conseillerSiteWeb = req.body.emetteur.ConseillerSite;
		if ((req.body.emetteur.ConseillerId != undefined) && (req.body.emetteur.ConseillerId != '')) {
			EmissionEo.conseillerId = req.body.emetteur.ConseillerId;
			var err = await checkConseiller(EmissionEo, req.decoded.id, req.body.emetteur);
		} else {
			var err = await AddConseillerContact(EmissionEo, req.decoded.id, req.body.emetteur);
			console.log(err)
			EmissionEo.conseillerId = err._id;
		}
	}

	EmissionEo.noteOperation = [];
	EmissionEo.documentJuridique = [];
	EmissionEo.documentComptable = [];
	/*console.log(req.body.remboursement)*/
	for (var item of req.body.noteOperation) {
		var err = await createPdfFile(item, EmissionEo, 1);
		if (err) {
			console.log(err);
			return res.status(500).json(err)
		}
		else {
			console.log("fini")
		}
	}
	for (var item of req.body.documentJuridique) {
		var err = await createPdfFile(item, EmissionEo, 2);
		if (err) {
			console.log(err);
			return res.status(500).json(err)
		}
		else {
			console.log("fini")
		}
	}
	for (var item of req.body.documentComptable) {
		var err = await createPdfFile(item, EmissionEo, 3);
		if (err) {
			console.log(err);
			return res.status(500).json(err)
		}
		else {
			console.log("fini")
		}
	}
	/*console.log(req.body.remboursement)*/

	if (req.body.acces.myContacts.length > 0) {
		req.body.acces.myContacts.forEach((elContact) => {
			generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + req.body.emission + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', elContact, "Yanvestee | Invitation", async function () {

			});
		})
	}
	EmissionEo.accesEmission = [];
	if (req.body.acces.myContacts.length > 0) {
		var temporaryContactArray = []
		for (var item of req.body.acces.myContacts) {
			var temporaryContact = await User.findOne({ email: item });
			temporaryContactArray.push(temporaryContact._id)
		}
		EmissionEo.accesEmission = req.body.acces.myContacts.length > 0 ? temporaryContactArray : [];
		console.log("EmissionEo.accesEmission")
	}

	if (req.body.acces.ToggleCheckedPublic) {
		EmissionEo.private = true;


	}
	if (req.body.acces.myExtrenalContacts.length > 0) {
		User.findById(req.decoded.id).exec(async function (err, myEmmetteur) {
			for (var item of req.body.acces.myExtrenalContacts) {
				console.log("Create contact")
				var err = await createExternalContact(EmissionEo, item, myEmmetteur);
				if (err) {
					console.log("err createExternalContact");
					console.log(err);
					return res.status(500).json(err)
				}
				else {
					saveEm(EmissionEo, req, res);
				}
			}
		})
	}
	else {
		saveEm(EmissionEo, req, res);
	}



}
exports.invitContacts = async (req, res) => {

	User.findById(req.decoded.id).populate("contacts").exec(async function (err, thisUser) {
		for (var item of req.body.contacts.myExtrenalContacts) {
			var err = await checkNewContact(item, thisUser, req.body.emission);
			if (err) {
				console.log(err);
				return res.status(500).json(err)
			}
			else {
				console.log("fini")
			}
		}
		if (req.body.contacts.myContacts.length > 0) {
			req.body.contacts.myContacts.forEach((elContact) => {
				generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + req.body.emission + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', elContact, "Yanvestee | Invitation", async function () {
					return res.status(200).json({ message: "Offre ajouté avec succès", data: "invIc" });
				});
			});
			/*generateTemplate.generateTemplateMail(offreContactMail.generateOffreInstitClubMail(req.body.emission, process.env.FRONTENDURL), req.body.contacts.myContacts, "Offre IC envoyée ", async function () {*/

		} else {
			return res.status(200).json({ message: "Offre ajouté avec succès", data: "invIc" });
		}

	})

}
function checkNewContact(newContact, thisUser, myemission) {
	return new Promise(resolve => {
		User.findOne({ email: newContact.email }).exec(async function (err, result) {
			if (result != null) {
				let obj = thisUser.contacts.find(o => o.email === newContact.email);
				if (obj != undefined) {
					console.log("cette personne est deja un deja contact")
					resolve();
				} else {
					var user = {
						email: result.email,
						societe: result.societe,
						firstname: result.firstname,
						lastname: result.lastname,
						tel: result.tel,
						idUser: result.id
					}
					var contact = new Contact(user);
					contact.save(function (err) {
						if (err) {
							console.log(err);
							return resolve(err);
						}
						thisUser.contacts.push(contact._id);
						thisUser.save(function (err) {
							if (err) {
								console.log(err);
								return resolve(err);
							}
							generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + myemission + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', req.body.contacts.myContacts, "Yanvestee | Invitation", user.email, "Offre IC envoyée ")
							resolve();
						})
					})
				}



			} else {
				var newUser = new User(user);
				//Add role as investor
				newUser.email = newContact.email;
				newUser.firstname = newContact.email;
				newUser.lastname = "";
				newUser.role = "Investisseur";
				//Add id role
				var checkRole = await Role.findOne({ name: "Public", type: "Investisseur" });
				//Check if Role exist

				if (checkRole != null) {
					newUser.roleId = checkRole._id;
					newUser.categorie = checkRole.name;
				}

				//Generate Password
				var pwd = generatorPassowrd.generate({
					length: 10,
					numbers: true,
					uppercase: true
				});
				newUser.password = newUser.generateHash(pwd);
				//Set User verif
				newUser.verif = true;
				//Save new user
				newUser.save(function (err) {
					if (err) {
						console.log(err);
						return resolve(err);
					}

					var contact = new Contact(user);
					contact.idUser = newUser._id;
					contact.firstname = newContact.email;
					contact.email = newContact.email;
					contact.societe = newContact.email;
					contact.lastname = "";
					//Add Contact
					contact.save(function (err) {
						//Push id to contacts
						thisUser.contacts.push(contact._id);
						thisUser.save(function (er) {
							if (er) {
								resolve(er);
							}
							//Send Mail
							//generateTemplate.generateTemplateMail(offreContactandOfferMail.generateOffreInstitClubMail(myemission, process.env.FRONTENDURL, contact.firstname, pwd), newUser.email, "Offre IC envoyée ",function(){
							generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour vous faciliter la consultation, nous vous avons déjà crée un compte dont il faudra completer les informations dans votre profil. Vos identifiants sont :</p><p></p><ul><li>Votre adresse email : " + newUser.email + "</li><li>Votre mot de passe : " + pwd + " </li></ul><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + myemission + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', newUser.email, "Yanvestee | Invitation", function () {
								resolve();
							})

						})
					});
				});
			}
		})
	})
}
function AddConseillerContact(myEmission, myUser, myConseiller) {
	return new Promise(resolve => {
		User.findById(myUser).populate("contacts").exec(async function (err, thisUser) {
			//If new user
			var user = {
				email: myConseiller.ConseillerEmail.toLowerCase(),
				societe: myConseiller.ConseillerSociete,
				firstname: myConseiller.ConseillerNom,
				lastname: myConseiller.ConseillerPrenom,
				tel: myConseiller.ConseillerPhone,
			}
			var newUser = new User(user);
			//Add role as investor
			newUser.role = "Emetteur";
			//Add id role
			var checkRole = await Role.findOne({ name: "Conseil", type: "Emetteur" });
			//Check if Role exist
			if (checkRole != null) {
				newUser.roleId = checkRole._id;
				newUser.categorie = checkRole.name;
			}

			//Generate Password
			var pwd = generatorPassowrd.generate({
				length: 10,
				numbers: true,
				uppercase: true
			});
			newUser.password = newUser.generateHash(pwd);
			//Set User verif
			newUser.verif = true;
			//Save new user
			newUser.save(function (err) {
				if (err) {
					console.log(err);
					return resolve(err);
				}

				var contact = new Contact(user);
				contact.idUser = newUser._id;

				//Add Contact
				contact.save(function (err) {
					//Push id to contacts
					thisUser.contacts.push(contact._id);
					thisUser.save(function (er) {
						if (er) {
							resolve(er);
						}
						myEmission.conseillerId = newUser._id;
						//Send Mail
						generateTemplate.generateTemplateMail(htmlContact.generateHtmlContact(newUser.email, pwd), newUser.email, "Création d'un compte", function () {
							//I should change message to i18n : Ahmed mzoughi
							// return res.status(200).json({ message: "Votre contact a été ajouté avec succès", data: contact });
							resolve(newUser, contact._id);
						});

					})
				});
			});
		})
	})
}
function checkConseiller(myEmission, myUser, myConseiller) {
	return new Promise(resolve => {
		User.findById(myUser).populate("contacts").exec(function (err, thisUser) {

			var templateAccept = `
			<p>Bonjour </p><p>${thisUser.firstname} ${thisUser.lastname} a publié une nouvelle offre sur Yanvestee Markeplace et vous invite à la consulter. <a href="https://jinvestis-int.sintegralabs.net/listing/emprunt-obligataire">Voir details</a></p>
			<p>Pour toute autre question, n’hésitez pas à nous contacter par mail <a href="helpdesk@yanvestee.com">helpdesk@yanvestee.com</a></p>
			<p>A très bientôt</p>
			<p>L'équipe Yanvestee</p>
			`

			let obj = thisUser.contacts.find(o => o.email === myConseiller.ConseillerEmail);
			if (obj != undefined) {
				generateTemplate.generateTemplateMail(templateAccept, myConseiller.ConseillerEmail, "Création d'un compte", function () {
					resolve();
				});
			} else {
				var newContact = new Contact();
				newContact.email = myConseiller.ConseillerEmail.toLowerCase();
				newContact.societe = myConseiller.ConseillerSociete;
				newContact.firstname = myConseiller.ConseillerNom;
				newContact.lastname = myConseiller.ConseillerPrenom;
				newContact.tel = myConseiller.ConseillerPhone;
				newContact.idUser = myConseiller.ConseillerId;
				newContact.tags = ['Conseil']
				newContact.save(function (err) {
					if (err) {
						resolve(err);
					} else {
						thisUser.contacts.push(newContact._id)
						thisUser.save(function (err) {
							generateTemplate.generateTemplateMail(templateAccept, newContact.email, "Création d'un compte", function () {
								if (err) {
								} else {
									resolve();
								}
							});

						})
					}
				})
			}
		})
	})
}


function saveEmission(myEmission, data, fixeValue, index) {
	return new Promise(resolve => {
		var finalEmission = new Emission(myEmission);
		finalEmission.category = index;
		finalEmission.RembourseMaturity = data.maturity;
		finalEmission.period = data.maturity * 12;
		finalEmission.endEmission = moment().add(3, 'M');
		finalEmission.RembourseModaliteAmortie = data.amortieCheckbox;
		finalEmission.RembourseModaliteInFine = data.InFineCheckbox;
		finalEmission.RembourseFrequenceCouponYear = data.AnuelleCheckbox;
		finalEmission.RembourseFrequenceCouponSemester = data.SemestrielleCheckbox;
		finalEmission.RembourseYearGraceBool = data.GraceYearBoolean;
		finalEmission.RembourseYearGraceAmount = data.GraceYearValue;
		if (fixeValue) {
			finalEmission.rateFixedNominal = data.TauxFixeValue;
			finalEmission.RembourseTauxFixeBool = true;
			finalEmission.RembourseTauxFixeAmount = data.TauxFixeValue;
			finalEmission.RembourseTauxVariableBool = false;
		} else {
			finalEmission.rateFixedNominal = data.TauxVaraibleValue;
			finalEmission.RembourseTauxFixeBool = false;
			finalEmission.RembourseTauxVariableBool = true;
			finalEmission.RembourseTauxVariableAmount = data.TauxVaraibleValue;
		}

		finalEmission.save(function (err) {
			if (err) {
				resolve(err)
			} else {
				resolve(null);
			}
		})
	})
}
async function saveEm(EmissionEo, req, res) {
	req.body.remboursement.categories.forEach(async (item, index) => {
		if (item.TauxFixeBoolean) {
			var err = await saveEmission(EmissionEo, item, true, index);
		}
		if (item.TauxVariableBoolean) {
			var err = await saveEmission(EmissionEo, item, false, index);
		}
		if (err) {
			console.log(err);
			return res.status(500).json(err)
		}
	});
	return res.status(200).json({ message: "Emission enregistrée" });
}

function createPdfFile(data, myEmission, myFileType) {
	return new Promise(resolve => {
		serviceHelper.uploadFile(data.myData, data.name, function (filePath, fileName, fileType) {
			var currentDate = new Date();
			var newResource = new Resource();
			newResource.relativePath = filePath;
			newResource.name = fileName;
			newResource.mimeType = fileType;
			newResource.uploadPath = '/static/resources/' + fileName;
			newResource.creationDate = currentDate;
			newResource.save(function (err, resou) {
				if (myFileType == 1) {
					myEmission.noteOperation.push(newResource._id);
				} else if (myFileType == 2) {
					myEmission.documentJuridique.push(newResource._id);
				} else if (myFileType == 3) {
					myEmission.documentComptable.push(newResource._id);
				}

				resolve();
			});
		});
	})
}

function createExternalContact(myEmission, data, myEmmetteur) {
	return new Promise(resolve => {
		User.findOne({ email: data.email }).exec(function (err, foundUser) {
			if (foundUser) {
				if (myEmmetteur.contacts.indexOf(foundUser._id) > -1) {
					myEmission.accesEmission.push(foundUser.idUser)
					resolve()
				} else {
					myEmmetteur.contacts.push(foundUser.idUser);
					myEmmetteur.save(function (err) {
						if (err) {
							console.log(err);
						}
						console.log(myEmission)
						myEmission.accesEmission.push(foundUser.idUser)
						generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + myEmission.id + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', data.email, "Yanvestee | Invitation", function () {
							resolve()
						})

					})
				}

			} else {
				var obj = {
					societe: data.email,
					fonction: "",
					tags: ['prospect'],
					firstname: data.email,
					lastname: "",
					email: data.email,
					tel: "",
					mobile: ""
				}
				//Create user account
				createContactUser(obj, myEmmetteur, myEmission).then((user, idContact) => {
					console.log(myEmission)
					myEmission.accesEmission.push(user._id);
					//Acess Emisson
					// myEmmetteur.contacts.push(idContact);
					// myEmmetteur.save((errEm) => {
					// if (!errEm) {

					resolve();

					// }
					// })
				}, (err) => {

				});
			}
		})


	})
}
exports.createBT = (req, res) => {
	createEmission(req, res, "Billets de trésorerie");
}

exports.createCT = (req, res) => {
	createEmission(req, res, "Comptes à terme");
}

exports.createPL = (req, res) => {
	createEmission(req, res, "Pension livrée");
}

exports.isEmissionOwner = (req, res) => {
	User.findById(req.decoded.id).exec(async function (err, myUser) {
		if (err)
			return res.status(500).json({ message: err });

		var emission = await Emission.findById(req.body.emissionId);

		//Check if current emission

		if (emission.emetteurId.equals(myUser._id))
			return res.status(200).json({ message: "success", data: true });

		return res.status(200).json({ message: "success", data: false });

	});
}
//Delete emission
exports.deleteEmission = (req, res) => {
	try {
		//Delete emission by id
		Emission.update({ _id: req.body.idEmission }, { active: false }, function (err) {
			if (!err) {
				//I should change message to i18n : Ahmed mzoughi
				return res.status(200).json({ message: "Votre émission a été cloturé avec succès" });
			}
			else {
				return res.status(500).json({ message: "Error" });
			}
		});


	} catch (error) {
		console.log(error);
	}
}
//Extend endDate emission
exports.extendEndDateEmission = (req, res) => {
	try {
		//Get extend value must be 3 or 6
		var extendValue = req.body.extendPeriod;
		//Check if extendPeriod between 3 or 6
		if (extendValue == 3 || extendValue == 6) {
			//Get emission by id
			Emission.findById(req.body.emissionId).exec((err, emission) => {

				if (!emission)
					return res.status(500).json({ message: "Emission not found" });

				//Check if not extended period
				if (emission.extendPeriod >= 1)
					return res.status(500).json({ message: "Emission déja prolongé" });
				//Update end date
				emission.endEmission = moment(emission.endEmission).add(extendValue, 'M');
				emission.extendPeriod += 1;
				//Save changes
				emission.save((err, _emission) => {
					//throw error
					if (err)
						return res.status(500).json({ message: "Error", data: err });

					//return 
					return res.status(200).json({ message: "success", data: _emission });
				});

			});
		}
		else {
			return res.status(500).json({ message: "Error" });
		}
	} catch (error) {
		console.log(error);
	}
}
//Functions must removed from emissions
//Create contact user
async function createContactUser(user, currentUser, myemission) {

	return new Promise(async (resolve, reject) => {

		//If new user
		var newUser = new User(user);
		//Add role as investor
		newUser.role = "Investisseur";
		//Add id role
		var checkRole = await Role.findOne({ name: "Public", type: newUser.role });
		//Check if Role exist
		if (checkRole != null) {
			newUser.roleId = checkRole._id;
			newUser.categorie = checkRole.name;
		}

		//Generate Password
		var pwd = generatorPassowrd.generate({
			length: 10,
			numbers: true,
			uppercase: true
		});
		newUser.password = newUser.generateHash(pwd);
		//Set User verif
		newUser.verif = true;
		//Save new user
		newUser.save(function (err) {
			if (err) {
				console.log(err);
				return reject(err);
			}

			var contact = new Contact(user);
			contact.idUser = newUser._id;

			//Add Contact
			contact.save(function (err) {
				//Push id to contacts
				currentUser.contacts.push(contact._id);
				currentUser.save(function (er) {
					if (er) {
						reject(er);
					}
					//Send Mail
					generateTemplate.generateTemplateMail("<p>Bonjour</p><p>Vous avez reçu une invitation pour consulter une emission.</p><p>Pour vous faciliter la consultation, nous vous avons déjà crée un compte dont il faudra completer les informations dans votre profil. Vos identifiants sont :</p><p></p><ul><li>Votre adresse email : " + newUser.email + "</li><li>Votre mot de passe : " + pwd + "</li></ul><p>Pour voir plus de détails sur cette offre, veuillez cliquer sur ce lien : <a href='" + process.env.FRONTENDURL + "/marketPlace/emissionDetails?emissionId=" + myemission + "'>Consulter l'offre</a></p><p>Si vous rencontrez des problèmes, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt, </p><p>L'équipe Yanvestee</p>" + '<td><img src="cid:' + 'unique@kreata.ee' + '"/></td><br />', newUser.email, "Yanvestee | Invitation", function () {
						resolve(newUser, contact._id);
					})
					/*generateTemplate.generateTemplateMail(htmlContact.generateHtmlContact(newUser.email, pwd), newUser.email, "création d'un compte", function () {
						//I should change message to i18n : Ahmed mzoughi
						// return res.status(200).json({ message: "Votre contact a été ajouté avec succès", data: contact });
						resolve(newUser, contact._id);
					});*/

				})
			});
		});
	});

}
