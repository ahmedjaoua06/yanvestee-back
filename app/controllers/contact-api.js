var Users = require('./../models/users-model');
var Contact = require('./../models/contact-model');
var Roles = require('./../models/roles-model');
var generatorPassowrd = require('generate-password');
//Import Helpers Generate Template
var generateTemplate = require("../helpers/templateMail");
var htmlContact = require("../helpers/templateHTML/html_contact");

//Create Contact
exports.contact = async (req, res) => {
    try {
        //Get Current User
        Users.findById(req.decoded.id).populate("contacts").exec(async function (err, currentUser) {
            try {
                //Get User values from req body
                var user = req.body;
                //Check if user exist
                var checkUser = await Users.findOne({ email: user.email });
                //If user exist return user
                if (checkUser != null) {
                    //Check if user exist in list contact current user
                    var checkUserExist = currentUser.contacts.find(a => a.idUser.equals(checkUser._id));
                    //If exist in list
                    if (checkUserExist != null) {
                        return res.status(500).json({ message: "Ce contact existe déjà" });
                    }
                    //else user not exist
                    else {
                        var contact = new Contact(req.body);
                        contact.idUser = checkUser._id;
                        //Add Contact
                        contact.save(function (err) {
                            //Push id to contacts
                            currentUser.contacts.push(contact._id);
                            currentUser.save(function (er) {
                                return res.status(200).json({ message: "Votre contact a été ajouté avec succès", data: contact });
                            })
                        });
                    }
                }
                //Else create user
                else {
                    createContact(res, currentUser, user);
                }
            } catch (err) {
                console.log(err);
            }
        });
    }
    catch (error) {
        console.log(error)
    }
}
//Get All Contacts by IdUser
exports.getAllContactsByIdUser = (req, res) => {
    try {
        //Get Current User
        Users.findById(req.decoded.id).populate("contacts").exec(function (err, currentUser) {

            //Set Query
            var querry = { $and: [{ _id: { $in: currentUser.contacts } }] };
            //Check if filter tags exist
            if (req.body.tags) {
                //Get tags
                var tags = req.body.tags;
                var regex = tags.join("|");
                querry.$and.push({
                    tags: {
                        "$regex": regex,
                        "$options": "i"
                    }
                });
            }
            var querySearch = { $or: [] }
            //Check if search querry firstname
            if (req.body.searchQuerry.name) {
                console.log("Name");
                querySearch.$or.push({ firstname: { $regex: req.body.searchQuerry.name, $options: 'i' } });
                querry.$and.push(querySearch);
                querySearch.$or.push({ lastname: { $regex: req.body.searchQuerry.name, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry category
            if (req.body.searchQuerry.category) {
                querySearch.$or.push({ categorie: { $regex: req.body.searchQuerry.category, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry company
            if (req.body.searchQuerry.company) {
                querySearch.$or.push({ societe: { $regex: req.body.searchQuerry.company, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry sector
            if (req.body.searchQuerry.sector) {
                querySearch.$or.push({ sector: { $regex: req.body.searchQuerry.sector, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry fonction
            if (req.body.searchQuerry.fonction) {
                querySearch.$or.push({ fonction: { $regex: req.body.searchQuerry.fonction, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //push querry

            console.log("===== querySearch =======");
            console.log(querySearch);
            //Set Options
            var sortObject = { creationDate: -1 };
            var page = req.body.page != undefined ? parseInt(req.body.page) : 1,
                limitPage = req.body.limitPage != undefined ? parseInt(req.body.limitPage) : 10

            var options = {
                sort: sortObject,
                lean: true,
                page: page,
                limit: limitPage
            };

            //Get List Contact
            Contact.paginate(querry, options).then(function (result) {
                return res.status(200).json({ message: "success", data: result });
            });

        });

    }
    catch (error) {
        console.log(error)
    }
}
//Get All Contact
exports.getAllContactsByUser = (req, res) => {
    try {
        //Get Current User
        Users.findById(req.decoded.id).populate("contacts").exec(function (err, currentUser) {

            //Set Query
            var querry = { $and: [{ _id: { $in: currentUser.contacts } }] };
            //Check if filter tags exist
            var querySearch = { $or: [] }
            //Check if search querry firstname
            if (req.body.searchQuerry.name) {
                console.log("Name");
                querySearch.$or.push({ firstname: { $regex: req.body.searchQuerry.name, $options: 'i' } });
                querry.$and.push(querySearch);
                querySearch.$or.push({ lastname: { $regex: req.body.searchQuerry.name, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry category
            if (req.body.searchQuerry.category) {
                querySearch.$or.push({ categorie: { $regex: req.body.searchQuerry.category, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry company
            if (req.body.searchQuerry.company) {
                querySearch.$or.push({ societe: { $regex: req.body.searchQuerry.company, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry sector
            if (req.body.searchQuerry.sector) {
                querySearch.$or.push({ sector: { $regex: req.body.searchQuerry.sector, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            //Check if search querry fonction
            if (req.body.searchQuerry.fonction) {
                querySearch.$or.push({ fonction: { $regex: req.body.searchQuerry.fonction, $options: 'i' } });
                querry.$and.push(querySearch);
            }
            Contact.find(querry).exec((err, contacts) => {
                console.log("Contacts");
                console.log(contacts);
                return res.status(200).json({ message: "success", data: contacts });
            });
        });
    }
    catch (error) {
        console.log(error)
    }
}

//Update contact
exports.updateContact = (req, res) => {
    try {
        //Get Current User
        Users.findById(req.decoded.id).exec(function (err, currentUser) {
            //Id Contact
            var idContact = req.params.idContact;
            //Get contact from body request
            var contact = req.body;
            //Check if contact in list contact current user
            var contactExist = currentUser.contacts.find(a => a == idContact);
            //If contact exist
            if (contactExist) {
                //Find contact by id
                Contact.find({ _id: idContact }).exec((error, c) => {
                    if (!error) {
                        delete contact.email
                        //Save contact
                        Contact.findOneAndUpdate({ _id: idContact }, contact, (err, newContact) => {
                            if (err)
                                return res.status(500).json({ message: err });
                            //I should change message to i18n : Ahmed mzoughi
                            return res.status(200).json({ message: "mise à jour terminée avec succès" });

                        });
                    }
                });
            }
            //Access denied 
            else {
                return res.status(500).json({ message: "Accès refusé" });
            }
        });
    } catch (error) {
        console.log(error);
    }
}
//Get Contact By id
exports.getContactById = (req, res) => {
    try {
        //Get Current User
        Users.findById(req.decoded.id).exec(function (err, currentUser) {
            //Get IdContact from body request 
            var contactId = req.body.idContact;
            //Check if contact in list contact current user
            var contactExist = currentUser.contacts.find(a => a == contactId);
            //If contact exist
            if (contactExist) {
                Contact.findById(req.body.idContact).exec((err, contact) => {
                    if (err)
                        return res.status(500).json({ message: err });

                    //Return contact
                    return res.status(200).json({ message: "success", data: contact });
                });
            }
            //Access denied 
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }
        });

    } catch (error) {
        console.log(error);
    }
}
//Remove Contact
exports.deleteContact = (req, res) => {

    try {
        Users.findById(req.decoded.id).exec(function (err, currentUser) {
            let idContact = req.params.idContact;
            if (err)
                return res.status(500).json({ message: "user not found" });
            //Remove element from contacts
            currentUser.contacts = currentUser.contacts.filter(a => a != idContact);
            //Update current user
            currentUser.save(function (err) {
                //Delete item form collection contact
                Contact.remove({ _id: idContact }, function (err) {
                    if (!err) {
                        //I should change message to i18n : Ahmed mzoughi
                        return res.status(200).json({ message: "Votre contact a été supprimé avec succès" });
                    }
                    else {
                        return res.status(500).json({ message: "Error" });
                    }
                });

            });
        });
    } catch (error) {
        console.log(error)
    }

}
//Create Contact
createContact = async function (res, currentUser, user) {
    //If new user
    var newUser = new Users(user);
    //Add role as investor
    newUser.role = "Investisseur";
    //Add id role
    var checkRole = await Roles.findOne({ name: "Public", type: newUser.role });
    //Check if Role exist
    if (checkRole != null) {
        newUser.roleId = checkRole._id;
        newUser.categorie = checkRole.name;
    }

    //Generate Password
    var pwd = generatorPassowrd.generate({
        length: 10,
        numbers: true,
        uppercase: true
    });
    newUser.password = newUser.generateHash(pwd);
    //Set User verif
    newUser.verif = true;
    //Save new user
    newUser.save(function (err) {
        if (err) {
            console.log(err);
            return res.status(500).json({ error: err });
        }

        var contact = new Contact(user);
        contact.idUser = newUser._id;

        //Add Contact
        contact.save(function (err) {
            //Push id to contacts
            currentUser.contacts.push(contact._id);
            currentUser.save(function (er) {
                //Send Mail
                console.log("Send Email");
                generateTemplate.generateTemplateMail(htmlContact.generateHtmlContact(newUser.email, pwd), newUser.email, "création d'un compte", function () {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(200).json({ message: "Votre contact a été ajouté avec succès", data: contact });
                });

            })
        });
    });
}