require('dotenv').config();
//Import Models
var UserModel = require('./../models/users-model');
var InstitModel = require('./../models/instit-model');
var InvestmentModel = require('./../models/investment-model');
//Import service helper
var generateTemplate = require("../helpers/templateMail");
var inviInstitClubMail = require('../helpers/templateHTML/html_institClub');
var offreInstitClubMail = require('../helpers/templateHTML/html_offreIC');
var NotifControler = require('../controllers/notif-api');
//moment
var moment = require('moment');
moment().format();


//Create Offre investor
exports.createOffre = (req, res) => {
    try {
        //Check current user
        UserModel.findById(req.decoded.id).populate("roleId").exec(function (err, myUser) {
            var acceptRole = ["Assurance", "Asset Managers"];
            //Check if user is Investor
            if (myUser.roleId.type == "Investisseur") {
                var _roleName = acceptRole.find(function (elt) {
                    return elt == myUser.roleId.name;
                });
                //Check if user has permission
                if (_roleName != undefined) {
                    //Get Form value
                    var InstitInvestor = req.body;
                    InstitInvestor.investorId = myUser._id;
                    InstitInvestor = new InstitModel(InstitInvestor);
                    InstitInvestor.endEmission = InstitInvestor.availableDate;
                    InstitInvestor.save(function (err) {
                        if (err) {
                            console.log(err);
                            //I should change message to i18n : Ahmed mzoughi
                            return res.status(500).json({ message: "une erreur est survenue pendant l'ajout" });
                        }

                        //Send notification for all users have role emetteur expect users blacklist
                        UserModel.find({ role: "Emetteur", _id: { $nin: InstitInvestor.blacklist } }, function (err, users) {
                            var dataNotif = {
                                key: "CREATE_IC",
                                userSender: myUser._id,
                                idEmission: InstitInvestor._id,
                                content: 'a publié une offre sur Instit Club'
                            }

                            //loop users
                            users.forEach((v, i) => {
                                //Send Mail
                                var link = process.env.FRONTENDURL + "/instit-club/details?id=" + InstitInvestor._id + "&userId=" + v._id;
                                console.log()
                                generateTemplate.generateTemplateMail("<p>Bonjour " + v.firstname + "</p><p>Une nouvelle offre de liquidité d’un montant de " + InstitInvestor.amount + " million de dinar sur " + InstitInvestor.period + " jours vient d’être publiée sur Yanvestee.</p><p>L’offre est désormais visible sur l’instit club ou sur ce lien </p><p><a href='" + link + "'>" + link + "</a>. </p> <p>Vous pouvez y répondre et vous serez notifiés de son evolution en temps réel.</p><p>Si vous avez des questions, n’hésitez pas à nous contacter par mail: helpdesk@yanvestee.com</p><p>A très bientôt,</p><p>L'équipe Yanvestee</p>", v.email, "Invitation au Instit Club", function () {

                                });
                            });


                            if (dataNotif) {
                                var listUsers = [];
                                //Add Notification
                                NotifControler.CreateNotification(dataNotif, (notifID) => {
                                    users.forEach((el) => {
                                        listUsers.push({ notifId: notifID, userId: el._id });
                                    });
                                    NotifControler.CreateUsersNotification(listUsers, () => {
                                        //I should change message to i18n : Ahmed mzoughi
                                        return res.status(200).json({ message: "Ajouté avec succès", data: InstitInvestor });
                                    })
                                });
                            }



                        });

                    })
                }
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "Accès refusé" });
                }
            }
            //Else reject
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }
        });
    } catch (error) {
        console.log(error);
    }
}
//Get List Role emetteur users
exports.getListUserByRole = (req, res) => {
    try {
        //Check current user
        UserModel.findById(req.decoded.id).populate("roleId").exec(function (err, myUser) {
            //Check if current user is investor
            if (myUser.roleId.type == "Investisseur") {
                //Get List Category
                UserModel.find({ categorie: req.body.categorie }).exec(function (err, users) {
                    if (err) {
                        return res.status(500).json({ message: "error", data: err });
                    }
                    return res.status(200).json({ message: "success", data: users });
                });
            }
            //Access denied
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }
        });
    } catch (error) {
        console.log(error);
    }

}
//Get List offre by investor
exports.getListOffresByInvestor = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).exec(function (err, myUser) {
            var querry = { $and: [{ investorId: req.decoded.id }] };

            if (req.body.pageIndex != undefined) {
                var page = parseInt(req.body.pageIndex);
            } else {
                var page = 1;
            }
            if (req.body.limitPage != undefined) {
                var limitPage = parseInt(req.body.limitPage);
            } else {
                var limitPage = 10;
            }
            if (req.body.sortType != undefined) {
                var sortObject = {};
                var stype = req.body.sortType;
                var sdir = req.body.sortOrder;
                sortObject[stype] = sdir;
            } else {
                var sortObject = { creationDate: -1 };
            }

            var options = {
                sort: sortObject,
                populate: "blacklist investorId",
                lean: true,
                page: page,
                limit: limitPage
            };

            if (err)
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "une erreur est survenue" });
            //Find offres by id current user
            InstitModel.paginate(querry, options).then(async function (result) {
                if (err)
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: err });

                var listIdsOffers = result.docs.map(a => a._id);
                var listInvestments = await InvestmentModel.find({ offreicID: { $in: listIdsOffers }, active: true });
                result.docs.forEach((el) => {
                    // var elt = listObjIds.find(a => a.offreicID.equals(el._id));
                    var nbrInvestments = listInvestments.filter(a => a.offreicID.equals(el._id));
                    el.nbrInvestments = nbrInvestments.length;
                    // el.status = el ? el.status : 0;
                });
                return res.status(200).json({ message: "success", data: result });
            });
        });
    } catch (error) {
        console.log(error);
    }
}
//Get List offre by emetteur
exports.getListOffresByEmetteur = async (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {

            if (err)
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "une erreur est survenue" });

            //Check if user role not investor
            if (currentUser.roleId.type != "Investisseur") {
                //Get IC offers by status
                var offerIc = await InvestmentModel.aggregate(
                    [
                        { $match: { emetteurId: currentUser._id, active: true } },
                        { $lookup: { from: InstitModel.collection.name, localField: 'offreicID', foreignField: '_id', as: 'emission' } },
                        { $unwind: { path: '$emission', preserveNullAndEmptyArrays: true } },
                        {
                            $group:
                            {
                                _id: '$status',
                                count: { $sum: 1 }
                            }
                        }
                    ]
                );
                var currentDate = moment();
                var investments = await InvestmentModel.find({ emetteurId: { $eq: currentUser._id }, status: { $eq: 1 }, active: true }).distinct('offreicID');
                //Get New Offer ic offers
                var querry = { blacklist: { $nin: [req.decoded.id] }, _id: { $nin: investments }, endEmission: { $gte: currentDate }, active: true };
                var totalNewOffreIc = await InstitModel.find(querry).count();
                //Create obj result
                let _result = {
                    accepted: 0,
                    refused: 0,
                    inprogress: 0,
                    totat: 0,
                    new: 0
                }
                offerIc.forEach((el) => {
                    //Set count refused
                    _result.refused += el._id == 2 ? el.count : 0;
                    //Set count accepted
                    _result.accepted += el._id == 3 ? el.count : 0;
                    //Set count in progress
                    _result.inprogress += el._id == 1 ? el.count : 0;
                });

                _result.new = totalNewOffreIc;
                _result.totat = totalNewOffreIc + _result.refused + _result.accepted + _result.inprogress;

                switch (req.body.status) {
                    case 1:
                        getEmissionsICByEM(req, currentUser).then((result) => {
                            result.info = _result;
                            return res.status(200).json({ message: "success", data: result });
                        });
                        break;
                    case 2: //In progress offer
                        getEmissionsICByEM(req, currentUser).then((result) => {
                            result.info = _result;
                            return res.status(200).json({ message: "success", data: result });
                        });
                        break;
                    case 3: //Accepted offer
                        getEmissionsICByEM(req, currentUser).then((result) => {
                            result.info = _result;
                            return res.status(200).json({ message: "success", data: result });
                        });
                        break;
                    case 4://Refused offer
                        getEmissionsICByEM(req, currentUser).then((result) => {
                            result.info = _result;
                            return res.status(200).json({ message: "success", data: result });
                        });
                        break;
                    default://New offer && all
                        getAllEmissionIC(req, currentUser, investments).then((result) => {
                            result.info = _result;
                            console.log("=== New Offer && all ===");
                            console.log(result);
                            return res.status(200).json({ message: "success", data: result });
                        });
                }
            }
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }
        });

    } catch (error) {
        console.log(error);
    }
}
//Get offre by id
exports.getOffreById = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).exec((err, user) => {
            //return error
            if (err)
                return res.status(500).json({ message: err });

            //Check if user exist
            if (!user)
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Aucun utilisateur trouvé" });

            //Get offre by id
            InstitModel.findById(req.body.emissionId).populate({ path: 'investorId', select: 'email firstname lastname tel mobile societe fonction logo' }).exec((error, emission) => {

                if (error)
                    console.log(error)

                //if emission exist
                if (emission) {

                    //Check if current user has permission to see emission
                    var userPermission = emission.blacklist.find(a => a.equals(user._id));
                    if (userPermission)
                        //I should change message to i18n : Ahmed mzoughi
                        return res.status(500).json({ message: "accès refusé" });

                    //Return emission
                    return res.status(200).json({ message: "success", data: emission });
                }

                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Aucune émission trouvée" });

            });

        });
    } catch (error) {
        console.log(error)
    }
}
//Invest offre
exports.investmentIC = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(function (err, currentUser) {
            //if error
            if (err)
                return res.status(500).json({ message: "error", data: err });

            if (currentUser) {

                //Check role user
                if (currentUser.role != "Investisseur") {

                    var investEmetteurIC = req.body;
                    investEmetteurIC = new InvestmentModel(investEmetteurIC);
                    investEmetteurIC.emetteurId = currentUser._id;
                    investEmetteurIC.save((err, invIc) => {
                        if (err) {
                            console.log(err);
                            //I should change message to i18n : Ahmed mzoughi
                            return res.status(500).json({ message: "une erreur est survenue pendant l'ajout" });
                        }
                        else {

                            var dataNotif = {
                                key: "CREATE_IC",
                                userSender: currentUser._id,
                                idEmission: investEmetteurIC.offreicID,
                                content: 'A souscrire à votre offre ic'
                            }

                            generateTemplate.generateTemplateMail(offreInstitClubMail.generateOffreInstitClubMail(), currentUser.email, "Offre IC envoyée ", async function () {

                                var icOffer = await InstitModel.findById(investEmetteurIC.offreicID);

                                if (dataNotif) {
                                    //Add Notification
                                    NotifControler.CreateNotification(dataNotif, (notifID) => {
                                        var listUser = [{ notifId: notifID, userId: icOffer.investorId }]
                                        //Add notifications users
                                        NotifControler.CreateUsersNotification(listUser, () => {
                                            return res.status(200).json({ message: "Offre ajouté avec succès", data: invIc });
                                        });
                                    });
                                }



                            });

                        }
                    });

                }
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "accès refusé" });
                }


            }
        });
    }
    catch (error) {
        console.log(error)
    }

}
//Get List investment By offer
exports.getAllInvestmentICByIdOffre = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: "error", data: err });

            if (currentUser) {
                if (currentUser.role != "Emetteur") {

                    //Check if current user is owner of IC offre
                    var isOwner = await InstitModel.findById(req.body.offreId);

                    if (isOwner.investorId.equals(currentUser._id)) {
                        var querry = { $and: [{ offreicID: req.body.offreId }, { active: true }] };

                        if (req.body.pageIndex != undefined) {
                            var page = parseInt(req.body.pageIndex);
                        } else {
                            var page = 1;
                        }
                        if (req.body.limitPage != undefined) {
                            var limitPage = parseInt(req.body.limitPage);
                        } else {
                            var limitPage = 10;
                        }
                        if (req.body.sortType != undefined) {
                            var sortObject = {};
                            var stype = req.body.sortType;
                            var sdir = req.body.sortOrder;
                            sortObject[stype] = sdir;
                        } else {
                            var sortObject = { creationDate: -1 };
                        }

                        var options = {
                            sort: sortObject,
                            populate: "emetteurId",
                            lean: true,
                            page: page,
                            limit: limitPage
                        };

                        //Find investments
                        InvestmentModel.paginate(querry, options).then(function (result) {
                            if (err)
                                //I should change message to i18n : Ahmed mzoughi
                                return res.status(500).json({ message: err });

                            return res.status(200).json({ message: "success", data: result });
                        });

                    }
                    //Access denied
                    else {
                        //I should change message to i18n : Ahmed mzoughi
                        return res.status(500).json({ message: "accès refusé" });

                    }

                }
                //Access denied
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "accès refusé" });
                }
            }
        });
    } catch (error) {
        console.log(error)
    }
}
//Accept or Refuse offre IC
exports.acceptOrRefuseOffreIc = async (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: "error", data: err });

            //If current exist
            if (currentUser) {
                //Check if role not Emetteur
                if (currentUser.role != "Emetteur") {
                    //Check if current user is owner of IC offre
                    var isOwner = await InstitModel.findById(req.body.offreId);
                    //If user is owner of IC offre
                    if (isOwner.investorId.equals(currentUser._id)) {
                        //Get investment
                        InvestmentModel.findById(req.body.idInvest).exec((errorInvest, invest) => {
                            if (errorInvest) {
                                return res.status(500).json({ message: err });
                            }
                            invest.status = req.body.status;
                            invest.updateDate = new Date();
                            invest.save((errUP, newInvest) => {


                                if (errUP)
                                    return res.status(500).json({ message: err });


                                //Change status offer
                                InstitModel.findById(req.body.offreId).exec((errInstit, offer) => {

                                    if (!errInstit) {

                                        //Check if status is accepted
                                        if (req.body.status == 3) {
                                            offer.active = false;
                                            var currentDate = moment();
                                            currentDate.set('second', 0);
                                            currentDate.set('minute', 0);
                                            currentDate.set('hour', 0);
                                            offer.endEmission = currentDate;
                                            offer.save((errUpdateOffer) => {
                                                if (errUpdateOffer)
                                                    return res.status(500).json({ message: errUpdateOffer });

                                                var dataNotif = {
                                                    key: "CREATE_IC",
                                                    userSender: currentUser._id,
                                                    idEmission: req.body.offreId,
                                                    content: 'a accepté votre souscription'
                                                }

                                                if (dataNotif) {
                                                    //Add Notification
                                                    NotifControler.CreateNotification(dataNotif, (notifID) => {
                                                        var listUser = [{ notifId: notifID, userId: invest.emetteurId }]
                                                        NotifControler.CreateUsersNotification(listUser, () => {
                                                            var users = [];
                                                            users.push(invest.emetteurId);
                                                            var data = {
                                                                invest: invest,
                                                                investor: currentUser,
                                                                offer: offer
                                                            }
                                                            sendEmailUserIC(data, users, 3, () => {
                                                                //Create querry
                                                                var query = { offreicID: req.body.offreId, _id: { $ne: req.body.idInvest } };
                                                                var data = {
                                                                    invest: invest,
                                                                    investor: currentUser,
                                                                    offer: offer,
                                                                    req: req.body

                                                                }
                                                                //Value to update
                                                                var valueUpdate = { $set: { status: 2 } };

                                                                InvestmentModel.updateMany(query, valueUpdate, (errUpdate, raw) => {
                                                                    var queryInvestor = { offreicID: req.body.offreId, _id: { $ne: req.body.idInvest }, status: 2, active: true };


                                                                    InvestmentModel.find(queryInvestor).select("emetteurId").exec((err, invRefused) => {
                                                                        var _dataNotifRefus = {
                                                                            key: "Ref_IC",
                                                                            userSender: currentUser._id,
                                                                            idEmission: req.body.offreId,
                                                                            content: 'a refusé votre souscription'
                                                                        }
                                                                        NotifControler.CreateNotification(_dataNotifRefus, (notifRefID) => {
                                                                            var listUser = [];
                                                                            var _users = [];
                                                                            invRefused.forEach((el) => {
                                                                                _users.push(el.emetteurId);
                                                                                listUser.push({ notifId: notifRefID, userId: el.emetteurId })
                                                                            });


                                                                            NotifControler.CreateUsersNotification(listUser, () => {
                                                                                sendEmailUserIC(data, _users, 2, () => {
                                                                                    if (errUpdate)
                                                                                        return res.status(500).json({ message: err });
                                                                                    return res.status(200).json({ message: "la mise à jour a été effectuée avec succés", data: newInvest });
                                                                                });
                                                                            });

                                                                        });

                                                                    });

                                                                });
                                                            });

                                                        })
                                                    });
                                                }
                                            });
                                        }
                                        else {

                                            var dataNotif = {
                                                key: "CREATE_IC",
                                                userSender: currentUser._id,
                                                idEmission: req.body.offreId,
                                                content: 'a refusé votre souscription'
                                            }
                                            if (dataNotif) {
                                                //Add Notification
                                                NotifControler.CreateNotification(dataNotif, (notifID) => {
                                                    var listUser = [{ notifId: notifID, userId: invest.emetteurId }]
                                                    NotifControler.CreateUsersNotification(listUser, () => {
                                                        return res.status(200).json({ message: "la mise à jour a été effectuée avec succés", data: newInvest });

                                                    });
                                                });
                                            }
                                        }

                                    }
                                });

                            });
                        })
                    }
                }
                //Access denied
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "accès refusé" });
                }
            }

        });
    } catch (error) {
        console.log("Instit API Method => acceptOrRefuseOffreIc");
        console.log(error);
    }
}
//Check if current user owner of IC offre
exports.checkOffreIc = async (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: err });
            if (currentUser) {
                if (currentUser.role != "Emetteur") {
                    var isOwner = await InstitModel.findById(req.body.offreId);
                    if (isOwner.investorId.equals(currentUser._id))
                        return res.status(200).json({ message: "success", data: true });
                    return res.status(200).json({ message: "success", data: false });
                }
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "Accès refusé" });
                }
            }
        });

    } catch (error) {
        console.log(error)
    }
}
//Check if already invested
exports.checkInvested = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: err });

            if (currentUser) {
                if (currentUser.role != "Investisseur") {
                    var isOwner = await InvestmentModel.find({ offreicID: req.body.offreId, emetteurId: currentUser._id, active: true });
                    if (isOwner.length > 0)
                        return res.status(200).json({ message: "success", data: true });
                    return res.status(200).json({ message: "success", data: false });
                }
                else {
                    //I should change message to i18n : Ahmed mzoughi
                    return res.status(500).json({ message: "Accès refusé" });
                }
            }


        });
    } catch (error) {
        console.log(error)
    }
}
//Get offres emetteurs
exports.getInfoOffers = async (req, res) => {
    try {

        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: err });

            if (!currentUser)
                return res.status(500).json({ message: "user not found" });

            if (currentUser.role != "Investisseur") {

                var offerIc = await InvestmentModel.aggregate(
                    [
                        { $match: { emetteurId: currentUser._id } },
                        { $lookup: { from: InstitModel.collection.name, localField: 'offreicID', foreignField: '_id', as: 'emission' } },
                        { $unwind: { path: '$emission', preserveNullAndEmptyArrays: true } },
                        {
                            $group:
                            {
                                _id: '$status',
                                count: { $sum: 1 },
                                totalAmount: {
                                    $sum:
                                    {
                                        $cond: [{ $in: ['$status', [1, 2, 3]] }, '$emission.amount', 0]
                                    }
                                },
                                totalAmountAcceptedOffer: {
                                    $sum:
                                    {
                                        $cond: [{ $eq: ['$status', 3] }, '$emission.amount', 0]
                                    }
                                },
                                totalAmountRefusedOffer: {
                                    $sum:
                                    {
                                        $cond: [{ $eq: ['$status', 2] }, '$emission.amount', 0]
                                    }
                                },
                                totalAmountWaitingOffer: {
                                    $sum:
                                    {
                                        $cond: [{ $eq: ['$status', 1] }, '$emission.amount', 0]
                                    }
                                },
                            }
                        }
                    ]
                );

                //Create obj result
                let result = {
                    accepted: 0,
                    refused: 0,
                    inprogress: 0,
                    amount: 0,
                    amountAC: 0,
                    amountRF: 0,
                    amountWA: 0,
                }

                offerIc.forEach((el) => {
                    //Set count refused
                    result.refused += el._id == 2 ? el.count : 0;
                    //Set count accepted
                    result.accepted += el._id == 3 ? el.count : 0;
                    //Set count in progress
                    result.inprogress += el._id == 1 ? el.count : 0;
                    //Amount
                    result.amount += el.totalAmount;
                    result.amountAC += el.totalAmountAcceptedOffer;
                    result.amountRF += el.totalAmountRefusedOffer;
                    result.amountWA += el.totalAmountWaitingOffer;
                });

                return res.status(200).json({ data: result });
            }
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }
        });
    } catch (error) {
        console.log(error)
    }



}
//Get List all offers
exports.getListAlloffres = (req, res) => {
    try {
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {

            if (err)
                return res.status(500).json({ message: err });

            if (!currentUser)
                return res.status(500).json({ message: "user not found" });

            if (currentUser.role != "Investisseur") {

                var querry = { $and: [{ emetteurId: currentUser._id }] };

                if (req.body.pageIndex != undefined) {
                    var page = parseInt(req.body.pageIndex);
                } else {
                    var page = 1;
                }
                if (req.body.limitPage != undefined) {
                    var limitPage = parseInt(req.body.limitPage);
                } else {
                    var limitPage = 10;
                }
                if (req.body.sortType != undefined) {
                    var sortObject = {};
                    var stype = req.body.sortType;
                    var sdir = req.body.sortOrder;
                    sortObject[stype] = sdir;
                } else {
                    var sortObject = { creationDate: -1 };
                }

                var options = {
                    select: "status rateIC type typerate creationDate active",
                    sort: sortObject,
                    populate: {
                        path: "offreicID",
                        populate: {
                            path: "investorId",
                            select: 'firstname lastname societe'
                        },
                        select: 'amount period availableDate creationDate'
                    },
                    lean: true,
                    page: page,
                    limit: limitPage
                };

                //Find investments
                InvestmentModel.paginate(querry, options).then(function (result) {
                    if (err)
                        //I should change message to i18n : Ahmed mzoughi
                        return res.status(500).json({ message: err });

                    return res.status(200).json({ message: "success", data: result });
                });

            }
            else {
                //I should change message to i18n : Ahmed mzoughi
                return res.status(500).json({ message: "Accès refusé" });
            }

        });
    } catch (error) {
        console.log(errpr)
    }

}
//Cancel offer
exports.cancelOfferEM = (req, res) => {
    try {
        //Get current user
        UserModel.findById(req.decoded.id).populate('roleId').exec(async function (err, currentUser) {
            if (err)
                return res.status(500).json({ message: err });
            var querry = { emetteurId: currentUser._id, offreicID: req.body.idEmission };
            //Get investment by currentUser and idEmission
            var getInvestments = await InvestmentModel.find(querry);
            if (!getInvestments)
                return res.status(500).json({ message: "Investment not found" });



            var valueUpdate = { $set: { active: false } };

            InvestmentModel.updateMany(querry, valueUpdate, (err, investment) => {
                if (err)
                    return res.status(500).json({ message: "une erreur est survenue pendant la modification" });

                return res.status(200).json(({ message: "Success" }));
            });
        });
    } catch (error) {
        console.log(error);
    }
}
//Functions
async function getAllEmissionIC(req, currentUser, inv) {
    return new Promise(async (resolve) => {
        var currentDate = moment();
        listObjIds = await InvestmentModel.find({ emetteurId: currentUser._id, active: true });

        var querry = { blacklist: { $nin: [req.decoded.id] }, endEmission: { $gte: currentDate } };

        if (req.body.pageIndex != undefined) {
            var page = parseInt(req.body.pageIndex);
        } else {
            var page = 1;
        }
        if (req.body.limitPage != undefined) {
            var limitPage = parseInt(req.body.limitPage);
        } else {
            var limitPage = 10;
        }
        if (req.body.sortType != undefined) {
            var sortObject = req.body.sortType;

        } else {
            var sortObject = { creationDate: -1 };
        }

        var options = {
            sort: sortObject,
            populate: "investorId",
            lean: true,
            page: page,
            limit: limitPage
        };
        //Find offres by id current user
        InstitModel.paginate(querry, options).then(async function (result) {
            var listIdsOffers = result.docs.map(a => a._id);
            var listInvestments = await InvestmentModel.find({ offreicID: { $in: listIdsOffers }, active: true });
            //Set status
            result.docs.forEach((el) => {
                var elt = listObjIds.find(a => a.offreicID.equals(el._id));
                var nbrInvestments = listInvestments.filter(a => a.offreicID.equals(el._id));
                el.nbrInvestments = nbrInvestments.length;
                el.status = elt ? elt.status : 0;

            });
            resolve(result);
        });
    });

}
async function getEmissionsICByEM(req, currentUser) {
    var currentDate = moment();
    return new Promise(async (resolve) => {
        var listObjIds;
        switch (req.body.status) {
            case 1:
                listObjIds = await InvestmentModel.find({ emetteurId: currentUser._id, active: true }).select("offreicID -_id");
                break;
            case 2:
                listObjIds = await InvestmentModel.find({ emetteurId: currentUser._id, status: 1, active: true }).select("offreicID -_id");
                break;
            case 3:
                listObjIds = await InvestmentModel.find({ emetteurId: currentUser._id, status: 3, active: true }).select("offreicID -_id");
                break;
            default:
                listObjIds = await InvestmentModel.find({ emetteurId: currentUser._id, status: 2, active: true }).select("offreicID -_id");
                break;
        }

        var ids = listObjIds.map(a => a.offreicID);

        if (req.body.status == 1) {
            var querry = { _id: { $nin: ids }, blacklist: { $nin: [req.decoded.id] }, endEmission: { $gte: currentDate }, active: true };
        }
        else {
            var querry = { _id: { $in: ids }, blacklist: { $nin: [req.decoded.id] } };
        }

        if (req.body.pageIndex != undefined) {
            var page = parseInt(req.body.pageIndex);
        } else {
            var page = 1;
        }
        if (req.body.limitPage != undefined) {
            var limitPage = parseInt(req.body.limitPage);
        } else {
            var limitPage = 10;
        }
        if (req.body.sortType != undefined) {
            console.log(req.body.sortType);
            var sortObject = req.body.sortType;

        } else {
            var sortObject = { creationDate: -1 };
        }

        var options = {
            sort: sortObject,
            populate: "investorId",
            lean: true,
            page: page,
            limit: limitPage
        };
        //Find offres by id current user
        InstitModel.paginate(querry, options).then(async function (result) {
            var status = 0;
            switch (req.body.status) {
                case 1:
                    status = 0
                    break;
                case 2:
                    status = 1
                    break;
                case 3:
                    status = 3
                    break;
                default:
                    status = 2;
                    break;
            }
            var listIdsOffers = result.docs.map(a => a._id);
            var listInvestments = await InvestmentModel.find({ offreicID: { $in: listIdsOffers }, active: true });
            result.docs.forEach((el) => {
                var nbrInvestments = listInvestments.filter(a => a.offreicID.equals(el._id));
                el.nbrInvestments = nbrInvestments.length;
                el.status = status
            });


            resolve(result);
        });
    });
}

function sendEmailUserIC(data, users = [], status, callback) {

    //Loop users
    if (users.length > 0) {

        UserModel.find({ _id: { $in: users } }).exec((err, docs) => {

            docs.forEach((el) => {
                if (status != 3) {
                    var templateAccept = `
                <p>Bonjour ${el.firstname}</p><p>Votre proposition de ${data.invest.rateIC} % pour l’offre de liquidité de ${data.investor.societe} d’un montant de ${data.offer.amount} million sur ${data.offer.period} jours a été déclinée .</p><p><a href=''>Voir details</a>
                <p>Pour toute autre question, n’hésitez pas à nous contacter par mail <a href="helpdesk@yanvestee.com">helpdesk@yanvestee.com</a></p>
                <p>A très bientôt</p>
                <p>L'équipe Yanvestee</p>
                `
                    generateTemplate.generateTemplateMail(templateAccept, el.email, "Yanvestee | Votre offre de taux " + data.invest.rateIC, function () {
                        return callback();
                    });
                }
                else {
                    var templateAccept = `
                <p>Bonjour ${el.firstname}</p><p>Votre proposition de ${data.invest.rateIC} % pour l’offre de liquidité de ${data.investor.societe} d’un montant de ${data.offer.amount} million sur ${data.offer.period} jours a été acceptée .</p><p><a href=''>Voir details</a>
                <p>Pour toute autre question, n’hésitez pas à nous contacter par mail <a href="helpdesk@yanvestee.com">helpdesk@yanvestee.com</a></p>
                <p>A très bientôt</p>
                <p>L'équipe Yanvestee</p>
                `
                    generateTemplate.generateTemplateMail(templateAccept, el.email, "Yanvestee | Votre offre de taux " + data.invest.rateIC, function () {
                        return callback();
                    });
                }

            });
        });
    }
    else {
        return callback();
    }
}
