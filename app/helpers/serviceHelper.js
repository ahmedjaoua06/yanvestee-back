var fs = require('fs');
var crypto = require('crypto');
var ListFAQ = [
    {
        question: 'Qui sommes nous?',
        answer: `Yanvestee est une plateforme qui met en relation des investisseurs et des émetteurs. Nous offrons également des solutions de gestion de produits financiers destinés aux professionnels du marché.
    Yanvestee est une plateforme100% digitale qui assure le listing des produits financiers régulés.`
    },
    {
        question: 'Qui sont vos partenaires?',
        answer: `Nos partenaires sont, principalement, les banques, les sociétés de leasing et les institutions de micro finance.  Nous sélectionons, soigneusement, les partenaires avec lesquels nous travaillons pour fournir les services les plus appropriés à nos clients. Nous ne sommes cependant pas en mesure de fournir une garantie quant à la solvabilité de ces partenaires.
        Nos partenaires étant des institutions financières qui traitent les données personnelles de leurs clients, elles assumment pleinement la responsabilité du traitement confidentiel de vos données conformément à la réglementation en vigueur.`
    },
    {
        question: `Offrez-vous des services de conseil en investissement?`,
        answer: `Non. Nous insistons sur le fait que, Yanvestee ne fournit pas de conseil en investissement concernant des produits particuliers ou des émetteurs partenaires. Toutes information utile concernant un émetteur, une banque ou un produit existe sur le site Yanvestee.Yanvestee n'offre pas de services d'intermédiation financière ni de conseils en investissement . Nous n’avons ne disposons d'aucune pas d’informations concernant votre situation financière ni vos objectifs d’investissement, nous ne sommes donc pas en mesure de vous fournir de tels conseils de quelque nature que ce soit. Nous vous sommes reconnaissants de votre compréhension.`
    },
    {
        question: `Vos services sont-ils uniquement en ligne ?`,
        answer: `Oui.  via la plateforme WEB : YANVESTEE. Les renseignements, aides et définitions sont disponibles sur notre le  site WEB : www.yanvestee.com. La sélection de produits se fait, uniquement en ligne. Les émetteurs ou leurs conseils prendrons contact avec le client pour la suite. Vous recevez ensuite les documents contractuels à imprimer et signer – si besoin.`
    },
    {
        question: `Comment ouvrir un compte?`,
        answer: `Yanvestee vous permet de visualiser des offres de placements et d'investissement d'institutions financières établies en Tunisie – en ligne. Les démarches à suivre sont simples : Afin d’ouvrir votre compte, veuillez vous inscrire sur notre site www.yanvestee.com. Vous recevrez par la suite vos identifiants par email et pourrez accèder à la plateforme. Vous serez alors en mesure de visualiser des produits parmi la gamme d'instruments de nos partenaires et d'entrer en relation avec eux. En règle générale, l’intégralité du processus, de l’inscription à la confirmation de l’ouverture de votre compte, prend au maximum deux semaines.`
    },
    {
        question: 'Comment se rémunère yanvestee?',
        answer: `Yanvestee ne facture aucun frais pour les investisseurs.
        Les frais d'ouverture et de tenue de comptes grand public sont gratuits
        Yanvestee reçoit perçoit une commission des institutions financières partenaires pour son activité de listing de produits et de mise en relation.`
    },
    {
        question: 'Investissement min max?   ',
        answer: `Le montant maximum d’investissement varie en fonction de chaque produit. Veuillez noter que certains émetteurs partenaires limitent le montant minimum/maximum des fonds pouvant être déposés investis. Ce montant minimum est habituellement le montant minimum exigé par la réglementation en vigueur. Pour les montants maximum, il est impossible d’excéder le montant maximum fixé et affiché sur la fiche produit .ce montant, quelque soit la maturité. Plus d’informations sont disponibles dans la fiche d’infomation de chaque produit.`
    },
    {
        question: 'A quelles conditions sont soumis les produits?',
        answer: `Yanvestee liste uniquement des produits régulés. Les conditions de chaque produits sont disponibles dans la fiche d’information produit ou la dataroom, ainsi que dans les Conditions générales des émetteurs partenaires. Vous devez par ailleurs accepter les conditions générales de Yanvestee.`
    },
    {
        question: `Offrez-vous des services d'intermédiation financière?`,
        answer: `Non. Nous insistons sur le fait que, Yanvestee ne fournit aucun service .Toutes information utile concernant un émetteur , une banque ou un produit existe sur le site Yanvestee.Yanvestee n'offre ni des services d'intermédiation financière ni de conseils en investissement . Nous n’avons ne disposons d'aucune pas d’informations concernant votre situation financière ni vos objectifs d’investissement, nous ne sommes donc pas en mesure de vous fournir de tels conseils de quelque nature que ce soit. Nous vous sommes reconnaissants de votre compréhension.`
    },
    {
        question: `Qu'est ce qu'un compte à terme?`,
        answer: `Un compte à terme (aussi appelé dépôt à terme) est un produit d’épargne permettant de placer ses fonds pour un taux d’intéret et une durée de placement déterminés au début de la relation contractuelle. L'échéance du compte à terme peut aller de 3 mois à 5 ans. `
    },
    {
        question: `Qu'est ce qu'un certificat de dépot ?`,
        answer: `
        Qui peut en bénéficier ?
        
        Toute personne physique ou morale titulaire d’un compte.
        
        Caractéristiques
        Nature : titres de créances au nom du souscripteur et émis au pair
        Montant : minimum multiple de 500 000dt
        Période : De 10 jours au moins et à 5 ans au plus avec une durée multiple de 10 jours, de mois ou d’année;
        Rémunération : rémunération à taux fixe pour les durées inférieures ou égales à un an, fixe ou variable pour les durées supérieures à un an
        Doivent être inscrits en comptes auprès d'un établissement de crédit.
        Remarques:
        Les certificats de dépôt que yanvestee liste pour les compte des emetteurs ne peuvent être ni remboursés par anticipation ni rachetés.
        Les intérêts sont servis en brut et sont assujettis à une retenue à la source au taux en vigueur.`
    },
    {
        question: `Qu'est ce qu'un billet de trésorerie?`,
        answer: `Les Billets de Trésorerie sont des titres de créance émis par les Sociétés Anonymes ayant un capital minimum de 1 Million de Dinars, pour un montant multiple de Cinquante Mille Dinars et une durée multiple de 10 jours et inférieure à 5 ans . Les Billets de Trésorerie à moins d'un an ne peuvent être rémunérés qu'à taux fixe.`
    },
    {
        question: `Qu'est ce qu'un Emprunt obligataire?`,
        answer: `Nature des titres : Titres de créance.
        - Forme des obligations : Les obligations seront nominatives.
        - Catégorie des titres : Ordinaire.
        - La législation sous laquelle les titres sont créés : Code des sociétés commerciales, livre 4, titre1, sous titre 5 chapitre 3 : des obligations. `
    },
    {
        question: `Avantage des comptes à terme?`,
        answer: `Avec un compte à terme, le remboursement du capital initial ainsi que le versement des intérêts sont garantis ; en effet, ni le capital ni les intérêts ne sont sujets à un risque de taux ou un risque de prix. Aussi longtemps que le compte à terme est libellé en Dinar Tunisien, il n’existe aucun de risque de change. Un risque de change est toutefois possible dans le cas où le dépôt à terme est dans une devise autre que le dinar. Un risque d’insolvabilité des émetteurs demeure mais nous sommes assez séléctifs dans le choix de nos partenaires émetteurs.
        En principe, vous ne pouvez pas accéder aux fonds investis sur votre compte à terme avant sa maturité. Cependant, Yanvestee propose également des produits autorisant la résiliation anticipée, sans pénalité – veuillez noter qu’en cas de résiliation anticipée, il se peut que le taux d’intérêt soit réduit ou nul.
        Par ailleurs, le compte à terme vous garantit un taux d’intérêt fixe que vous percevrez, à la différence du livret d’épargne qui peut voir son taux d’intérêt périodiquement réévalué.
        En outre, vous bénéficiez de la gratuité des services fournis par Yanvestee. Aucun frais n’est prélevé pour l’ouverture et la gestion d’un compte.`
    },
    {
        question: `A quel moment le compte à terme commence à courir?`,
        answer: `La durée commence à courir une fois que nous avons reçu tous les documents nécessaires à l’ouverture du compte, que les fonds ont été transférés vers le compte de la Banque partenaire.`
    },
    {
        question: `Que se passe t-il à la date de maturité du compte à terme ?`,
        answer: `Vous décidez. A maturité, vous pouvez choisir de transférer les fonds vers un autre compte ou de prolonger le placement; en cas de prolongation, la durée et le taux d’intérêt en vigueur au moment de la prolongation sont appliqués.
        A maturité, vous êtes bien sûr libre de choisir de retirer vos fonds.`
    },
    {
        question: `Qui peut devenir investisseur sur yanvestee?`,
        answer: `La plateforme www.yanvestee.com est accessible aux résidents tunisiens, âgés d’au moins 18 ans et contractant en leur propre nom.

        Nous regrettons que les citoyens des États-Unis d’Amérique (US Persons) ainsi que les détenteurs d’une carte verte (toute personne considérée comme étant « états-unienne »), ne soient pas autorisés à accéder à nos offres en raison de la réglementation stricte régissant la transmission des données personnelles.`
    },
    {
        question: `Quels sont vos frais?`,
        answer: `En règle générale, les services fournis sont gratuits – nous prenons tous les coûts en charge. Ces services s’étendent de l'inscription,  l’ouverture et la gestion de votre Compte Yanvestee, confirmations de portefeuille, les notifications, les alertes et notre équipe de conseillers client. Aussi, L’ouverture et la gestion des comptes auprès de nos partenaires sont, en principe, gratuits.`
    },
    {
        question: `Quelle est la fiscalité des produits?`,
        answer: `Les intérêts sont servis en BRUT. Ils sont soumis à un taux de retenue à la source non libératoire d'impôts de 20% pour les Personnes Physiques et Morales résidentes en Tunisie  (réglementation en vigueur)`
    },
    {
        question: `Est que Yanvestee retient le prélèvement de l'impot à la source?`,
        answer: `Non – Yanvestee ne retiennent d’impôts sur vos intérêts perçus. Les revenus d’intérêts sont imposés conformément à la reglementation fiscale en vigueur. Une fois le compte arrivé à maturité, vous recevrez des documents de la Banque partenaire contenant toutes les informations nécessaires pour votre déclaration d’impôt sur le revenu.`
    },
    {
        question: `Quels sont les risques?`,
        answer: `Investir dans un dépôt à terme, un certificat de dépot ou un billet de trésorerie présente un risque limité pour le client. Ces produits sont par ailleurs soumis à un risque d’insolvabilité en cas de faillite de la Banque partenaire.
        Tant que vous investissez dans la même devise (dinar tunisien, par exemple), vous n’êtes exposé à aucun risque de change. Pour les dépôts à terme effectués dans une monnaie étrangère, de tels risques existent. Soyez conscient que des pertes peuvent se produire pour les dépôts effectués dans une devise étrangère (risque de change). Ces pertes peuvent généralement être compensées dans votre déclaration d’impôt sur le revenu.
        Il n’y a pas de risque entrepreneurial, le paiement des intérêts n’étant pas lié aux résultats de l’entreprise – à la différence des actions.D’autres risques peuvent survenir en raison de systèmes juridiques et fiscaux différents, ainsi que lors de contrats conclus dans une autre langue.`
    },
    {
        question: `Comment restez informé?`,
        answer: `Les informations concernant les offres de nos partenaires sont disponibles en ligne depuis notre site Internet ainsi que depuis notre application en ligne. Notre lettre d’information vous tient également informé de nos avancées et offres les plus récentes. Nous sommes ravis de communiquer à nos clients nos nouvelles fonctionnalités, les tendances actuelles du marché ainsi que les offres attractives de nos partenaires.`
    },
    {
        question: `Pourquoi vous demandez des documents complémentaires parfois?`,
        answer: `Les exigences spécifiques liées à l’ouverture d’un compte peuvent changer en fonction du partenaire et de la réglementation en vigueur. Les exigences spécifiques de chaque banque partenaire sont accessibles sur les pages produits et les Fiches d’information produit spécifiques.

        Les documents exigés pour l’ouverture d’un compte vous seront transmis pré-remplis (dans la mesure du possible) pour vous faciliter la tâche.`
    }
]
//Decode base 64 to image
decodeBase64Image = function (dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
    var response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }
    console.log("==== decodeBase64Image =====");
    console.log(matches);
    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}
//Upload file
exports.uploadFile = (file, filename, callback) => {

    try {
        // Decoding base-64 image
        // Source: http://stackoverflow.com/questions/20267939/nodejs-write-base64-image-file


        // Regular expression for image type:
        // This regular image extracts the "jpeg" from "image/jpeg"
        var imageTypeRegularExpression = /\/(.*?)$/;

        // Generate random string

        var seed = crypto.randomBytes(20);
        var uniqueSHA1String = crypto
            .createHash('sha1')
            .update(seed)
            .digest('hex');

        var imageBuffer = decodeBase64Image(file);
        console.log(imageBuffer);
        var userUploadedFeedMessagesLocation = './public/resources';


        //Check if dir is exist
        if (!fs.existsSync(userUploadedFeedMessagesLocation)) {
            fs.mkdirSync(userUploadedFeedMessagesLocation);
        }
        userUploadedFeedMessagesLocation += "/";

        // This variable is actually an array which has 5 values,
        // The [1] value is the real image extension
        var imageTypeDetected = imageBuffer
            .type
            .match(imageTypeRegularExpression);
        var uniqueRandomImageName = 'image-' + uniqueSHA1String + '.' +
            imageTypeDetected[1];


        var userUploadedImagePath = userUploadedFeedMessagesLocation +
            filename;
        console.log(imageBuffer);
        // Save decoded binary image to disk
        try {
            require('fs').writeFile(userUploadedImagePath, imageBuffer.data,
                function () {
                    console.log('DEBUG - feed:message: Saved to disk image attached by user:', userUploadedImagePath);
                    callback(userUploadedImagePath, filename, imageTypeDetected[1]);
                });
        }
        catch (error) {
            console.log('ERROR:', error);
        }

    }
    catch (error) {
        console.log('ERROR:', error);
    }

}
//Check if object is empty
exports.isEmpty = (obj) => {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
exports.getListFAQ = ListFAQ;