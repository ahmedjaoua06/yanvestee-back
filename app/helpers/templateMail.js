require('dotenv').config();
var nodeMailer = require("nodemailer");

//Generate Template Mail
exports.generateTemplateMail = async (html, user, subject, callback) => {
    //Config Send Mail
    let transporter = nodeMailer.createTransport({
        host: "ssl0.ovh.net",
        port:465,
        auth: {
            user: process.env.EMAIL_TEMP,
            pass: process.env.PWD_TEMP
        }
    });
    console.log(user)
    //Set Mail Options
    let mailOptions = {
        from: process.env.EMAIL_TEMP,
        to: user,
        subject: subject,
        html: html
    };
    mailOptions.attachments = [{
        filename: 'Webp.net-resizeimage.png',
        path: './specialFile/Webp.net-resizeimage.png',
        cid: 'unique@kreata.ee' //same cid value as in the html img src
    }];
    //Send Mail
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            callback();
        } else {
            console.log("Message Sent!");
            callback();
        }
    });
}