var express = require('express');
var router = express.Router();
var auth = require('./auth')
var contactController = require('../controllers/contact-api');

//Create Contact
router.post('/', auth.isLoggedIn, contactController.contact);
//Delete Contact by id
router.delete('/:idContact', auth.isLoggedIn, contactController.deleteContact);
//Get list Contact by idUser
router.post('/getContact', auth.isLoggedIn, contactController.getAllContactsByIdUser);
//Get Contact by id
router.post('/getContactById', auth.isLoggedIn, contactController.getContactById); 
//Update Contact
router.put('/updateContact/:idContact', auth.isLoggedIn, contactController.updateContact );
//Get all contact
router.post('/getAllContact', auth.isLoggedIn, contactController.getAllContactsByUser);
module.exports = router;