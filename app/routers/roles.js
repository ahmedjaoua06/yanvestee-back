var Roles = require('./../models/roles-model');

Roles.find({}).exec(function(err, roles){
	if(roles.length==0){
		Roles.findOne({name : "Banque"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Banque";
				newRole.type = "Emetteur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Pension livrée",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})

		Roles.findOne({name : "Leasing, factoring"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Leasing, factoring";
				newRole.type = "Emetteur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:false},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})
		Roles.findOne({name : "IMF"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "IMF";
				newRole.type = "Emetteur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})
		Roles.findOne({name : "Conseil"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Conseil";
				newRole.type = "Emetteur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})
		Roles.findOne({name : "Corporates"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Corporates";
				newRole.type = "Emetteur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:true},{name:"Update",able:true},{name:"Read",able:true},{name:"Delete",able:true}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})

		Roles.findOne({name : "Asset Managers"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Asset Managers";
				newRole.type = "Investisseur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})

		Roles.findOne({name : "Assurance"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Assurance";
				newRole.type = "Investisseur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})

		Roles.findOne({name : "Avertis"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Avertis";
				newRole.type = "Investisseur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})

		Roles.findOne({name : "Corporates Investisseur"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Corporates";
				newRole.type = "Investisseur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})


		Roles.findOne({name : "Public"}, function(err, role){
			if (err) console.log(err);
			if (role ==null){
				var newRole = new Roles(); 
				newRole.name = "Public";
				newRole.type = "Investisseur";
				var elements = [
				{name:"Emprunt obligataire",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Certificat de dépôt",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Billets de trésorerie",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Comptes à terme",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:true},{name:"Delete",able:false}]},
				{name:"Pension livrée",actions:[{name:"Create",able:false},{name:"Update",able:false},{name:"Read",able:false},{name:"Delete",able:false}]},
				]
				newRole.elements = elements;
				newRole.save(function(err) {
					if (err) throw err;
					console.log("Le role " + newRole.name + " a été ajouté avec succès");
				});
			}
		})
	}
})
