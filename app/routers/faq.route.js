var express = require('express');
var router = express.Router();
var auth = require('./auth')
var faqController = require('../controllers/faq-api');

faqController.initListFaq();

//Get list users by role
router.post('/getFAQ', auth.isLoggedIn, faqController.getAllFAQ);
module.exports = router;