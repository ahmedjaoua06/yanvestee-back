var express = require('express');
var router = express.Router();
var auth = require('./auth');
var notifsController = require('../controllers/notif-api');

//Get Notification
router.post('/getNotifications', auth.isLoggedIn, notifsController.GetNotificationUserByIdUser);
//Get all notification 
router.post('/getAllNotification', auth.isLoggedIn, notifsController.getAllNotification);
//Chage status notif
router.post('/changeStatusNotif', auth.isLoggedIn, notifsController.notifSeenByUser);
module.exports = router;