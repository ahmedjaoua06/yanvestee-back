var express = require('express');
var router = express.Router();
var auth = require('./auth')
var institClubController = require('../controllers/instit-api');

//Create offre instit club
router.post('/create', auth.isLoggedIn, institClubController.createOffre);
//Get list users by role
router.post('/getListUserByRole', auth.isLoggedIn, institClubController.getListUserByRole);
//Get list offres by investor
router.post('/getMyOffres', auth.isLoggedIn, institClubController.getListOffresByInvestor);
//Get list offres by emetteur
router.post('/getListOffresByEM', auth.isLoggedIn, institClubController.getListOffresByEmetteur);
//Get offre by id
router.post('/getOffreById', auth.isLoggedIn, institClubController.getOffreById);
//Investment offre
router.post('/investmentIC', auth.isLoggedIn, institClubController.investmentIC);
//Get List investment
router.post('/getListInvestment', auth.isLoggedIn, institClubController.getAllInvestmentICByIdOffre);
//Accept or Refuse investment
router.post('/acceptRefuseInvestment', auth.isLoggedIn, institClubController.acceptOrRefuseOffreIc);
//Check if current user is owner of IC offre
router.post('/checkOffreIc', auth.isLoggedIn, institClubController.checkOffreIc);
//Check if already invested
router.post('/checkInvested', auth.isLoggedIn, institClubController.checkInvested);
//Get all offres by emetteur
router.post('/getlistOffersByEmetteur', auth.isLoggedIn, institClubController.getListAlloffres);
//Get info offre
router.post('/getInfoOffers', auth.isLoggedIn, institClubController.getInfoOffers);
//Cancel offer
router.post('/cancelOfferEM', auth.isLoggedIn, institClubController.cancelOfferEM);
module.exports = router;