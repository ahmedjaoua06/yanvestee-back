var express = require('express');
var router = express.Router();
var auth = require('./auth');
var investmentController = require('../controllers/investment-api');

router.post('/createInvestment', auth.isLoggedIn, investmentController.createInvestment);
router.post('/myInvestments', auth.isLoggedIn, investmentController.investmentList);
router.post('/updateInvestment', auth.isLoggedIn, investmentController.updateInvestment);
router.post('/myinvestmentsInvestor', auth.isLoggedIn, investmentController.myinvestmentsInvestor);

module.exports = router;