var express = require('express');
var router = express.Router();
var secret = '12311'; //token secret
var jwt = require('jsonwebtoken'); // to generate and decrypt token
var mongoose = require('mongoose');
var Users = require('./../models/users-model.js');
var users = require('./users.js');
var emissions = require('./emission.js');
var roles = require('./roles.js');
var faqRoute = require('./faq.route');
//Import Router
var institRouter = require('./instit-club.route');
var contactRouter = require('./contact.route');
var investments = require('./investment.js');
var notifications = require('./notifs.route');
var email = require('./email');

router.get('/version', function (req, res) {
    return res.status(200).json({ project: "yanvestee", version: "0.1.0" })
});
router.post("/login", (req, res, next) => {
    if ((req.body.email == undefined) || (req.body.password == undefined)) {
        return res.status(401).json({ error: "Un email et un mot de passe sont nécessaire pour l'authentification" });
    } else if ((req.body.email.length == 0) || (req.body.password.length == 0)) {
        return res.status(401).json({ error: "Un email et un mot de passe sont nécessaire pour l'authentification" });
    }

    Users.findOne({ 'email': req.body.email.toLowerCase() }, function (err, user) {
        // if there are any errors, return the error before anything else
        if (err)
            return res.status(401).json(err);

        // if no user is found, return the message
        if (!user)
            return res.status(401).json({ error: 'Login ou mot de passe incorrect' });


        // if the user is deleted, his active property will be false. Return the message.
        if (user.active == false)
            return res.status(401).json({ error: 'Login ou mot de passe incorrect' }); // req.flash is the way to set flashdata using connect-flash          

        if (user.verif == false)
            return res.status(401).json({ error: "Ce compte n'est pas encore verifé" }); // create the loginMessage and save it to session as flashdata

        // if the user is found but the password is wrong
        if (!user.validPassword(req.body.password))
            return res.status(401).json({ error: 'Login ou mot de passe incorrect' }); // create the loginMessage and save it to session as flashdata

        // all is well, return successful user

        var token = jwt.sign({ id: user._id, role: user.role }, secret);

        console.log("The user " + user.firstname + " has logged in. Session : " + user._id);
        return res.status(200).json({ message: "Authentification réussie", token: token, role: user.role, roleId: user.roleId, firstname: user.firstname, lastname: user.lastname, type: user.categorie, idUser: user._id });
    });
});


router.use('/users', nextLogin, users);
router.use('/emissions', nextLogin, emissions);
//Contact router
router.use('/contact', nextLogin, contactRouter);
//Instit router
router.use('/institClub', nextLogin, institRouter);
//Investments 
router.use('/investments', nextLogin, investments);
//email
router.use('/mailer', nextLogin, email);
//Notifications
router.use('/notifications', nextLogin, notifications);
//FAQ
router.use('/faq', nextLogin, faqRoute);
function nextLogin(req, res, next) {
    console.log('nextLogin');
    next();
}

module.exports = router;
