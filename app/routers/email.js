var express = require('express');
var router = express.Router();
var auth = require('./auth');
var emailController = require('../controllers/email-api');

router.post('/sendEmail', auth.isLoggedIn, emailController.email);

module.exports = router;