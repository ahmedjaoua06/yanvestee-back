var jwt = require('jsonwebtoken');
var secret = "12311"
exports.isLoggedIn = (req, res, next) => {

	var token = req.headers['authorization'];

	if (token) {
		token = token.replace('Bearer ', '')
	}
	// if user is authenticated in the session, carry on
	jwt.verify(token, secret, function(err, decoded) {
		if (err) {
			console.log(err)
			return res.status(401).json({ success: false, message: 'Impossible de reconnaitre le token d\'authentification'});
		} else {
			
			req.decoded = jwt.decode(token);
			return next();
		}
	});
}