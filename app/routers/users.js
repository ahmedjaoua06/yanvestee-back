var express = require('express');
var router = express.Router();
var auth = require('./auth')
var userController = require('../controllers/users-api');

router.get('/', function(req,res){
	return res.status(200).json({message : "message recu"})
});
router.post('/', userController.createUser);
router.post('/confirm', userController.confirmUser);
router.get('/profile',auth.isLoggedIn, userController.getProfile);
router.put('/profile',auth.isLoggedIn, userController.updateProfile);
router.get('/getMenu',auth.isLoggedIn, userController.getMenu);
router.post('/sendTokenForgotPassword', userController.sendTokenForgotPassword);
router.post('/forgotPassword', userController.forgotPassword);
router.post('/updatePassword',auth.isLoggedIn, userController.updatePassword);
// router.post('/createContact',auth.isLoggedIn, userController.contact);
router.post('/uploadImage',auth.isLoggedIn, userController.uploadFileUserProfile);
router.post('/getUserByEmail',auth.isLoggedIn, userController.getUserByEmail);
//Update outstading
router.post('/updateOutStanding', auth.isLoggedIn, userController.updateOutStanding);


module.exports = router;
