var express = require('express');
var router = express.Router();
var auth = require('./auth');
var emissionController = require('../controllers/emission-api');


router.post('/createCD',auth.isLoggedIn, emissionController.createCD);
router.post('/createCT',auth.isLoggedIn, emissionController.createCT);
router.post('/createBT',auth.isLoggedIn, emissionController.createBT);
router.post('/createEO',auth.isLoggedIn, emissionController.createEO);
router.post('/myEmissions',auth.isLoggedIn, emissionController.emissionList);
router.post('/emissionMarket',auth.isLoggedIn, emissionController.emissionMarket);
router.post('/dealClub',auth.isLoggedIn, emissionController.dealClub);
router.get('/getEmission/:emissionId',auth.isLoggedIn, emissionController.getEmission);
router.post('/isEmissionOwner', auth.isLoggedIn, emissionController.isEmissionOwner );
router.post('/deleteEmission', auth.isLoggedIn, emissionController.deleteEmission);
router.post('/extendEndDateEmission', auth.isLoggedIn, emissionController.extendEndDateEmission);
router.post('/invitContacts', auth.isLoggedIn, emissionController.invitContacts);
router.post('/emissionByPeriod', auth.isLoggedIn, emissionController.emissionByPeriod);
router.post('/sendMessage', auth.isLoggedIn, emissionController.sendMailToContact);

module.exports = router;
