FROM node:8

WORKDIR /app

ADD ./package.json .

RUN npm install --production

ADD ./ .

# Modifiy the exposed port
EXPOSE 3000

# Modify the main-file.js
CMD ["npm", "start"]
