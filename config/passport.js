var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');

var Users = require('./../app/models/users-model');

 passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
function(req, email, password, done) { // callback with email and password from our form
    Users.findOne({ 'email' :  email.toLowerCase() }, function(err, muuser) {
        // if there are any errors, return the error before anything else
        if (err)
            return done(err);

        // if no user is found, return the message
        if (!user )
            return done(null, false, 'Login ou mot de passe incorrect'); 

        // if the user is deleted, his active property will be false. Return the message.
        if (user.active == false)
             return done(null, false, 'Login ou mot de passe incorrect'); // req.flash is the way to set flashdata using connect-flash          
        
        if (user.verified == false )
            return done(null, false, "Ce compte n'est pas encore verifé"); // create the loginMessage and save it to session as flashdata

        // if the user is found but the password is wrong
        if (!user.validPassword(password) )
            return done(null, false, 'Login ou mot de passe incorrect.'); // create the loginMessage and save it to session as flashdata
       
        // all is well, return successful user
        return done(null, user);
    });
}));
module.exports = passport;